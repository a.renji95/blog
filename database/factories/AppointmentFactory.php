<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Appointment::class, function (Faker $faker) {
    return [
        'patient_id' => \App\Models\User::where('role_id',3)->first()->id,
        'doctor_id' => \App\Models\User::where('role_id',2)->first()->id,
        'shift_id' => 1,
        'date' => \Carbon\Carbon::now()->modify('+7 hours')->format('Y-m-d'),
        'status_id' => 1,
        'description' => ''
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Service::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->randomNumber()
    ];
});

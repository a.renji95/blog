<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Invoice::class, function (Faker $faker) {
    return [
        'status_id' => 1,
        'patient_id' => \App\Models\User::where('role_id',3)->first()->id,
        'appointment_id' => \App\Models\Appointment::first()->id,
        'amount' => $faker->randomNumber(),
        'payment_date' => $faker->date('Y-m-d')
    ];
});

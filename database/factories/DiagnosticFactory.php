<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Diagnostic::class, function (Faker $faker) {
    return [
        'appointment_id' => \App\Models\Appointment::first()->id,
        'description' => $faker->sentence
    ];
});

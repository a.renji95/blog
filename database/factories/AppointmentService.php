<?php

use Faker\Generator as Faker;

$factory->define(App\Models\AppointmentService::class, function (Faker $faker) {
    return [
        'appointment_id' => \App\Models\Appointment::first()->id,
        'service_id' => \App\Models\Service::first()->id
    ];
});

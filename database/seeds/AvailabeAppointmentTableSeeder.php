<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableAppointment;
use Carbon\Carbon;

class AvailabeAppointmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        AvailableAppointment::truncate();
        Schema::enableForeignKeyConstraints();

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 1,
            'date' => Carbon::parse('15-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 1,
            'date' => Carbon::parse('16-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 1,
            'date' => Carbon::parse('17-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 2,
            'date' => Carbon::parse('18-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 2,
            'date' => Carbon::parse('19-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 2,
            'date' => Carbon::parse('20-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 3,
            'date' => Carbon::parse('21-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 3,
            'date' => Carbon::parse('15-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 1,
            'shift_id' => 3,
            'date' => Carbon::parse('16-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 1,
            'date' => Carbon::parse('17-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 1,
            'date' => Carbon::parse('18-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 1,
            'date' => Carbon::parse('19-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 2,
            'date' => Carbon::parse('20-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 2,
            'date' => Carbon::parse('21-11-2018'),
            'status_id' => 2,
        ]);


        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 2,
            'date' => Carbon::parse('15-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 3,
            'date' => Carbon::parse('16-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 3,
            'date' => Carbon::parse('17-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 3,
            'date' => Carbon::parse('18-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 4,
            'date' => Carbon::parse('19-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 4,
            'date' => Carbon::parse('21-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 2,
            'shift_id' => 4,
            'date' => Carbon::parse('15-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 1,
            'date' => Carbon::parse('17-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 1,
            'date' => Carbon::parse('18-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 1,
            'date' => Carbon::parse('19-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 2,
            'date' => Carbon::parse('20-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 2,
            'date' => Carbon::parse('21-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 2,
            'date' => Carbon::parse('15-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 3,
            'date' => Carbon::parse('16-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 3,
            'date' => Carbon::parse('17-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 3,
            'date' => Carbon::parse('18-11-2018'),
            'status_id' => 3,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 4,
            'date' => Carbon::parse('19-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 4,
            'date' => Carbon::parse('20-11-2018'),
            'status_id' => 2,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 3,
            'shift_id' => 4,
            'date' => Carbon::parse('21-11-2018'),
            'status_id' => 3,
        ]);


        AvailableAppointment::create([
            'doctor_id' => 5,
            'shift_id' => 2,
            'date' => Carbon::parse('16-11-2018'),
            'status_id' => 1,
        ]);

        AvailableAppointment::create([
            'doctor_id' => 4,
            'shift_id' => 2,
            'date' => Carbon::parse('17-11-2018'),
            'status_id' => 3,
        ]);
    }
}

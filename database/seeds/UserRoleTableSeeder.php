<?php

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        UserRole::truncate();
        Schema::enableForeignKeyConstraints();

        UserRole::create([
            'role' => 'owner',
        ]);

        UserRole::create([
            'role' => 'doctor',
        ]);

        UserRole::create([
            'role' => 'patient',
        ]);
    }
}

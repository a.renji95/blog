<?php

use Illuminate\Database\Seeder;
use App\Models\Appointment;
use Carbon\Carbon;
use App\Models\Shift;
use App\Models\User;

class AppointmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Appointment::truncate();
        Schema::enableForeignKeyConstraints();

        $patients = User::where('role_id',3)->select('id')->get();
        $doctors = User::where('role_id',2)->select('id')->get();
        $shifts = Shift::all();

        $pIds = array();
        foreach ($patients as $patient){
            array_push($pIds,$patient->id);
        }
        $dIds = array();
        foreach ($doctors as $doctor){
            array_push($dIds,$doctor->id);
        }
        $sIds = array();
        foreach ($shifts as $shift){
            array_push($sIds,$shift->id);
        }

        for ($i = 0;$i <= 200; $i++){
            Appointment::create([
                'patient_id' => $pIds[array_rand($pIds, 1)],
                'doctor_id' => $dIds[array_rand($dIds, 1)],
                'shift_id' => $sIds[array_rand($sIds, 1)],
                'date' => Carbon::now()->modify('+7 hours')->format('Y-m-d'),
                'status_id' => 4,
                'description' => ''
            ]);
        }
    }
}

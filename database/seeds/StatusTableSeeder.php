<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Status::truncate();
        Schema::enableForeignKeyConstraints();

        Status::create([
            'name' => 'Booked',
        ]);

        Status::create([
            'name' => 'Waiting',
        ]);

        Status::create([
            'name' => 'Cancel',
        ]);

        Status::create([
            'name' => 'Done',
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Diagnostic;
use Faker\Factory as Faker;
use App\Models\Appointment;

class DiagnosticTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Diagnostic::truncate();
        Schema::enableForeignKeyConstraints();
        $faker = Faker::create();

        $appointmentIDs = Appointment::pluck('id')->toArray();
//        dd($appointmentIDs);
        for ($i = 0; $i < 100; $i ++) {
            Diagnostic::create([
                'appointment_id' => $appointmentIDs[array_rand($appointmentIDs)],
                'description' => 'Description'
            ]);
        }
    }
}

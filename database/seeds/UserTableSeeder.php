<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserRole;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        User::create([
            'name' => 'Nguyen Huy Vinh',
            'email' => 'vinhnh.uet.195@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => 2,
        ]);

        User::create([
            'name' => 'Nguyen Van Tranh',
            'email' => 'tranhnv@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => 2,
        ]);

        User::create([
            'name' => 'Vu Minh Tuan',
            'email' => 'tuanvm@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => 2,
        ]);

        User::create([
            'name' => 'Do Hoa An',
            'email' => 'andh@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => 2,
        ]);

        User::create([
            'name' => 'Bui Thi Thuy Tien',
            'email' => 'tienbtt@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01523650269',
            'address' => 'Hung Yen',
            'role_id' => 2,
        ]);

        User::create([
            'name' => 'Le Thi Thanh',
            'email' => 'thanhlt@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01553620269',
            'address' => 'Hai Duong',
            'role_id' => 2,
        ]);

        User::create([
            'name' => 'Vu Cong Khanh',
            'email' => 'khanhvc@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01553620256',
            'address' => 'Hai Duong',
            'role_id' => 3,
        ]);

        User::create([
            'name' => 'Tran Van Phong',
            'email' => 'phongtv@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01553620269',
            'address' => 'Hai Duong',
            'role_id' => 3,
        ]);

        User::create([
            'name' => 'Doan Thi Thanh Mai',
            'email' => 'maidtt@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01645638922',
            'address' => 'Nam Dinh',
            'role_id' => 3,
        ]);
    }
}

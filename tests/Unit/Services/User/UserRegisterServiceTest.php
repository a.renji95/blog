<?php

namespace Test\Unit\Services\User;

use App\Models\User;
use App\Repositories\User\UserEloquentRepository;
use App\Services\User\UserRegisterService;
use Illuminate\Support\Facades\Log;
use Mockery;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class UserRegisterServiceTest extends TestCase
{
    protected $userRegisterService;
    protected $userEloquentRepository;

//    public function setUp()
//    {
//        $this->userRegisterService = Mockery::mock('\App\Services\User\UserRegisterService');
//        $this->userEloquentRepository = Mockery::mock('\App\Repositories\User\UserEloquentRepository');
//        parent::setUp();
//    }

    public function testGetInstance()
    {
        $repository = \App::make(UserRegisterService::class);
        $this->assertNotNull($repository);
    }

    public function testRegisterSuccessShouldReceiveTrue(){
        DB::beginTransaction();
        $user = factory(User::class)->make();
        $data = array();
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['password'] = bcrypt('123456789');
        $repository = \App::make(UserRegisterService::class);
        $this->assertNotNull($repository);
        $result = $repository->register($data);
        $this->assertTrue($result);
        DB::rollBack();
    }


    //chua test dc
    public function testRegisterFailShouldReceiveFalse(){
        DB::beginTransaction();
        $user = factory(User::class)->create();
        $data = array();
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['password'] = bcrypt('123456789');
        $this->userEloquentRepository = \App::make(UserEloquentRepository::class);
        $this->userRegisterService = new UserRegisterService($this->userEloquentRepository);
        $this->assertNotNull($this->userRegisterService);
//        $this->userEloquentRepository->shouldReceive('insert')->andReturn(false);
//        $this->app->instance('App\Services\User\UserRegisterService',$this->userEloquentRepository);
//        $this->userRegisterService->shouldReceive('register')->andReturn(false);
//        $this->userRegisterService->shouldReceive('register')->with($data)->passthru();
        //$result = $this->userRegisterService->register($data);
        //$this->assertFalse($result);
        DB::rollBack();
    }
}

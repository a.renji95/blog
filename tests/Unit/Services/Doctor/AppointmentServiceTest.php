<?php

namespace Tests\Unit\Services\Doctor;

use App\Models\Appointment;
use App\Models\Diagnostic;
use App\Models\Service;
use App\Repositories\AppointmentService\AppointmentServiceEloquentRepository;
use App\Repositories\Diagnostic\DiagnosticEloquentRepository;
use App\Services\Doctor\AppointmentService;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class AppointmentServiceTest extends TestCase
{
    public function testAddDiagnosticServiceCreateSuccess(){
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create();
        $servicesData = factory(Service::class,3)->create();
        $diagnostic = 'test';
        $services = array();
        foreach ($servicesData as $service){
            array_push($services,$service->id);
        }
        $data = array();
        $data['appointment_id'] = $appointment->id;
        $data['services'] = $services;
        $data['diagnostic'] = $diagnostic;
        $appointmentService = \App::make(AppointmentService::class);
        $result = $appointmentService->addDiagnosticService($data);
        $expectedOutput = array(
            'error' => '',
            'success' => '<div class="alert alert-success">Appointment update successful!!</div>'
        );
        $this->assertEquals($expectedOutput,$result);
        DB::rollBack();
    }

    public function testAddDiagnosticServiceUpdateSuccess(){
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create();
        $servicesData = factory(Service::class,3)->create();
        $diagnostic_des = 'test';
        $diagnosticRepository = \App::make(DiagnosticEloquentRepository::class);
        $data = array();
        $data['appointment_id'] = $appointment->id;
        $data['description'] = $diagnostic_des;
        $diagnosticRepository->create($data);
        $services = array();
        foreach ($servicesData as $service){
            array_push($services,$service->id);
        }
        $data = array();
        $data['appointment_id'] = $appointment->id;
        $data['services'] = $services;
        $data['diagnostic'] = $diagnostic_des;
        $appointmentService = \App::make(AppointmentService::class);
        $result = $appointmentService->addDiagnosticService($data);
        $expectedOutput = array(
            'error' => '',
            'success' => '<div class="alert alert-success">Appointment update successful!!</div>'
        );
        $this->assertEquals($expectedOutput,$result);
        DB::rollBack();
    }

    public function testAddDiagnosticServiceFail(){
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create();
        $servicesData = factory(Service::class,3)->create();
        $diagnostic = 'test';
        $services = array();
        foreach ($servicesData as $service){
            array_push($services,$service->id);
        }
        $data = array();
        $data['appointment_id'] = 'failData';
        $data['services'] = $services;
        $data['diagnostic'] = $diagnostic;
        $appointmentService = \App::make(AppointmentService::class);
        $result = $appointmentService->addDiagnosticService($data);
        $expectedOutput = array(
            'error' => '<div class="alert alert-danger">Something wrong!! Please try again!!</div>',
            'success' => ''
        );
        $this->assertEquals($expectedOutput,$result);
        DB::rollBack();
    }

    public function testPaymentConfirmSuccess(){
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create();
        $servicesData = factory(Service::class,3)->create();
        $diagnostic_des = 'test';
        $diagnosticRepository = \App::make(DiagnosticEloquentRepository::class);
        $data = array();
        $data['appointment_id'] = $appointment->id;
        $data['description'] = $diagnostic_des;
        $diagnosticRepository->create($data);
        $appointmentServiceRepository = \App::make(AppointmentServiceEloquentRepository::class);
        $services = array();
        foreach ($servicesData as $service){
            array_push($services,$service->id);
            $data = array(
                'appointment_id' => $appointment->id,
                'service_id' => $service->id
            );
            $appointmentServiceRepository->create($data);
        }
        $data = array();
        $data['appointment_id'] = $appointment->id;
        $appointmentService = \App::make(AppointmentService::class);
        $result = $appointmentService->paymentConfirm($data);
        $expectedOutput = array(
            'error' => '',
            'success' => '<div class="alert alert-success">Payment Confirm Successful</div>'
        );
        $this->assertEquals($expectedOutput,$result);
        DB::rollBack();
    }

    public function testPaymentConfirmFail(){
        DB::beginTransaction();
        $appointmentService = \App::make(AppointmentService::class);
        $result = $appointmentService->paymentConfirm('a');
        $expectedOutput = array(
            'error' => '<div class="alert alert-danger">Something wrong!!!</div>',
            'success' => ''
        );
        $this->assertEquals($expectedOutput,$result);
        DB::rollBack();
    }

}

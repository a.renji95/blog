<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/26/2018
 * Time: 3:00 PM
 */

namespace Test\Unit\Repositories\AppointmentService;

use App\Models\AppointmentService;
use App\Repositories\AppointmentService\AppointmentServiceEloquentInterface;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class AppointmentEloquentRepositoryTest extends TestCase
{

    public function testGetInstance()
    {
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
    }

    public function testFind()
    {
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->create();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->find($appointment->id);
        $this->assertEquals($appointment->id, $check->id);
        DB::rollBack();
    }

    public function testCreate()
    {
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->make()->toArray();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->create($appointment);
        $this->assertNotNull($user_check);
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->create()->toArray();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update($appointment['id'], $appointment);
        $this->assertNotNull($check);
        DB::rollBack();
    }

    public function testUpdateFail(){
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->create()->toArray();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update('failedData', $appointment);
        $this->assertFalse($check);
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->create();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $repository->delete($appointment->id);
        $check = $repository->find($appointment->id);
        $this->assertNull($check);
        DB::rollBack();
    }

    public function testDeleteFail()
    {
        DB::beginTransaction();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->delete('failedData');
        $this->assertFalse($result);
        DB::rollBack();
    }

    public function testGetAll(){
        $number_record = AppointmentService::all()->count();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $number_record_check = $repository->getAll()->count();
        $this->assertEquals($number_record,$number_record_check);
    }

    public function testInsert(){
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->make();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->insert($appointment);
        $this->assertTrue($result);
        DB::rollBack();
    }

    public function testFindAppointServiceByServiceIdAndAppointmentId(){
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->create();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->findAppointServiceByServiceIdAndAppointmentId($appointment->appointment_id,$appointment->service_id);
        $this->assertEquals($appointment->id,$result->id);
        DB::rollBack();
    }

    public function testRemoveAppointmentServiceByAppointmentId(){
        DB::beginTransaction();
        $appointment = factory(AppointmentService::class)->create();
        $repository = \App::make(AppointmentServiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->removeAppointmentServiceByAppointmentId($appointment->appointment_id);
        $this->assertNotNull($result);
        $check = $repository->find($appointment->id);
        $this->assertNull($check);
        DB::rollBack();
    }
}

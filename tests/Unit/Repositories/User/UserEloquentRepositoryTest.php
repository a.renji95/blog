<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/26/2018
 * Time: 3:00 PM
 */

namespace Test\Unit\Repositories\User;

use App\Repositories\User\UserRepositoryInterface;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class UserEloquentRepositoryTest extends TestCase
{

    public function testGetInstance()
    {
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testFind()
    {
        DB::beginTransaction();
        $user = factory(User::class)->create();
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->find($user->id);
        $this->assertEquals($user->id, $user_check->id);
        DB::rollBack();
    }

    public function testCreate()
    {
        DB::beginTransaction();
        $users = factory(User::class)->make()->toArray();
        $users['password'] = bcrypt('123456');
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->create($users);
        $this->assertNotNull($user_check);
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $user = factory(User::class)->create()->toArray();
        $user['password'] = bcrypt('123456');
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->update($user['id'], $user);
        $this->assertNotNull($user_check);
        DB::rollBack();
    }

    public function testUpdateFail(){
        DB::beginTransaction();
        $user = factory(User::class)->create()->toArray();
        $user['password'] = bcrypt('123456');
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->update('failedData', $user);
        $this->assertFalse($user_check);
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        $user = factory(User::class)->create();
        $user->password = bcrypt('123456');
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $repository->delete($user->id);
        $user_check = $repository->find($user->id);
        $this->assertNull($user_check);
        DB::rollBack();
    }

    public function testDeleteFail()
    {
        DB::beginTransaction();
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->delete('failedData');
        $this->assertFalse($result);
        DB::rollBack();
    }

    public function testGetAll(){
        $number_record = User::all()->count();
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $number_record_check = $repository->getAll()->count();
        $this->assertEquals($number_record,$number_record_check);
    }

    public function testInsert(){
        DB::beginTransaction();
        $user = factory(User::class)->make();
        $repository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->insert($user);
        $this->assertTrue($result);
        DB::rollBack();
    }

}

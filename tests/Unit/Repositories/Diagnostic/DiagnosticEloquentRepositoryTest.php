<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/26/2018
 * Time: 3:00 PM
 */

namespace Test\Unit\Repositories\Diagnostic;

use App\Models\Diagnostic;
use App\Repositories\Diagnostic\DiagnosticEloquentInterface;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class DiagnosticEloquentRepositoryTest extends TestCase
{

    public function testGetInstance()
    {
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
    }

    public function testFind()
    {
        DB::beginTransaction();
        $diagnostic = factory(Diagnostic::class)->create();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->find($diagnostic->id);
        $this->assertEquals($diagnostic->id, $check->id);
        DB::rollBack();
    }

    public function testCreate()
    {
        DB::beginTransaction();
        $diagnostic = factory(Diagnostic::class)->make()->toArray();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->create($diagnostic);
        $this->assertNotNull($check);
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $diagnostic = factory(Diagnostic::class)->create()->toArray();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update($diagnostic['id'], $diagnostic);
        $this->assertNotNull($check);
        DB::rollBack();
    }

    public function testUpdateFail(){
        DB::beginTransaction();
        $diagnostic = factory(Diagnostic::class)->create()->toArray();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update('failedData', $diagnostic);
        $this->assertFalse($check);
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        $diagnostic = factory(Diagnostic::class)->create();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $repository->delete($diagnostic->id);
        $check = $repository->find($diagnostic->id);
        $this->assertNull($check);
        DB::rollBack();
    }

    public function testDeleteFail()
    {
        DB::beginTransaction();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->delete('failedData');
        $this->assertFalse($result);
        DB::rollBack();
    }

    public function testGetAll(){
        $number_record = Diagnostic::all()->count();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $number_record_check = $repository->getAll()->count();
        $this->assertEquals($number_record,$number_record_check);
    }

    public function testInsert(){
        DB::beginTransaction();
        $diagnostic = factory(Diagnostic::class)->make();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->insert($diagnostic);
        $this->assertTrue($result);
        DB::rollBack();
    }

    public function testFindDiagnosticByAppointmentId(){
        DB::beginTransaction();
        $diagnostic = factory(Diagnostic::class)->create();
        $repository = \App::make(DiagnosticEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->findDiagnosticByAppointmentId($diagnostic->appointment_id);
        $this->assertEquals($diagnostic->id,$result->id);
        DB::rollBack();
    }
}

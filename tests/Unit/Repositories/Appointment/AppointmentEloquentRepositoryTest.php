<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/26/2018
 * Time: 3:00 PM
 */

namespace Test\Unit\Repositories\Appointment;

use App\Models\User;
use App\Repositories\Appointment\AppointmentRepositoryInterface;
use App\Models\Appointment;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class AppointmentEloquentRepositoryTest extends TestCase
{

    public function testGetInstance()
    {
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testFind()
    {
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->find($appointment->id);
        $this->assertEquals($appointment->id, $user_check->id);
        DB::rollBack();
    }

    public function testCreate()
    {
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->make()->toArray();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->create($appointment);
        $this->assertNotNull($user_check);
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create()->toArray();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $user_check = $repository->update($appointment['id'], $appointment);
        $this->assertNotNull($user_check);
        DB::rollBack();
    }

    public function testUpdateFail(){
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create()->toArray();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $appointment_check = $repository->update('failedData', $appointment);
        $this->assertFalse($appointment_check);
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->create();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $repository->delete($appointment->id);
        $check = $repository->find($appointment->id);
        $this->assertNull($check);
        DB::rollBack();
    }

    public function testDeleteFail()
    {
        DB::beginTransaction();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->delete('failedData');
        $this->assertFalse($result);
        DB::rollBack();
    }

    public function testGetAll(){
        $number_record = Appointment::all()->count();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $number_record_check = $repository->getAll()->count();
        $this->assertEquals($number_record,$number_record_check);
    }

    public function testInsert(){
        DB::beginTransaction();
        $appointment = factory(Appointment::class)->make();
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->insert($appointment);
        $this->assertTrue($result);
        DB::rollBack();
    }

    public function testNumberRecordOfAppointmentByDoctorID(){
        DB::beginTransaction();
        $doctor = factory(User::class)->make()->toArray();
        $doctor['role_id'] = 3;
        $doctor['password'] = bcrypt('123456');
        $repository = \App::make(AppointmentRepositoryInterface::class);
        $this->assertNotNull($repository);
        $userRepository = \App::make(UserRepositoryInterface::class);
        $this->assertNotNull($userRepository);
        $doctor = $userRepository->create($doctor);
        $result = $repository->numberRecordOfAppointmentByDoctorID($doctor->id);
        $this->assertEquals(0,$result);
        DB::rollBack();
    }

}

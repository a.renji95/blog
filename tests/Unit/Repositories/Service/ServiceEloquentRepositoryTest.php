<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/26/2018
 * Time: 3:00 PM
 */

namespace Test\Unit\Repositories\Service;

use App\Models\Service;
use App\Repositories\Services\ServiceRepositoryInterface;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class InvoiceEloquentRepositoryTest extends TestCase
{

    public function testGetInstance()
    {
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
    }

    public function testFind()
    {
        DB::beginTransaction();
        $service = factory(Service::class)->create();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->find($service->id);
        $this->assertEquals($service->id, $check->id);
        DB::rollBack();
    }

    public function testCreate()
    {
        DB::beginTransaction();
        $service = factory(Service::class)->make()->toArray();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->create($service);
        $this->assertNotNull($check);
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $service = factory(Service::class)->create()->toArray();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update($service['id'], $service);
        $this->assertNotNull($check);
        DB::rollBack();
    }

    public function testUpdateFail(){
        DB::beginTransaction();
        $service = factory(Service::class)->create()->toArray();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update('failedData', $service);
        $this->assertFalse($check);
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        $service = factory(Service::class)->create();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $repository->delete($service->id);
        $check = $repository->find($service->id);
        $this->assertNull($check);
        DB::rollBack();
    }

    public function testDeleteFail()
    {
        DB::beginTransaction();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->delete('failedData');
        $this->assertFalse($result);
        DB::rollBack();
    }

    public function testGetAll(){
        $number_record = Service::all()->count();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $number_record_check = $repository->getAll()->count();
        $this->assertEquals($number_record,$number_record_check);
    }

    public function testInsert(){
        DB::beginTransaction();
        $service = factory(Service::class)->make();
        $repository = \App::make(ServiceRepositoryInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->insert($service);
        $this->assertTrue($result);
        DB::rollBack();
    }

}

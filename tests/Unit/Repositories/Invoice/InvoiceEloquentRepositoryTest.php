<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/26/2018
 * Time: 3:00 PM
 */

namespace Test\Unit\Repositories\Invoice;

use App\Models\Invoice;
use App\Repositories\Invoices\InvoiceEloquentInterface;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class InvoiceEloquentRepositoryTest extends TestCase
{

    public function testGetInstance()
    {
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
    }

    public function testFind()
    {
        DB::beginTransaction();
        $invoice = factory(Invoice::class)->create();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->find($invoice->id);
        $this->assertEquals($invoice->id, $check->id);
        DB::rollBack();
    }

    public function testCreate()
    {
        DB::beginTransaction();
        $invoice = factory(Invoice::class)->make()->toArray();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->create($invoice);
        $this->assertNotNull($check);
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $invoice = factory(Invoice::class)->create()->toArray();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update($invoice['id'], $invoice);
        $this->assertNotNull($check);
        DB::rollBack();
    }

    public function testUpdateFail(){
        DB::beginTransaction();
        $invoice = factory(Invoice::class)->create()->toArray();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $check = $repository->update('failedData', $invoice);
        $this->assertFalse($check);
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        $invoice = factory(Invoice::class)->create();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $repository->delete($invoice->id);
        $check = $repository->find($invoice->id);
        $this->assertNull($check);
        DB::rollBack();
    }

    public function testDeleteFail()
    {
        DB::beginTransaction();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->delete('failedData');
        $this->assertFalse($result);
        DB::rollBack();
    }

    public function testGetAll(){
        $number_record = Invoice::all()->count();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $number_record_check = $repository->getAll()->count();
        $this->assertEquals($number_record,$number_record_check);
    }

    public function testInsert(){
        DB::beginTransaction();
        $invoice = factory(Invoice::class)->make();
        $repository = \App::make(InvoiceEloquentInterface::class);
        $this->assertNotNull($repository);
        $result = $repository->insert($invoice);
        $this->assertTrue($result);
        DB::rollBack();
    }

}

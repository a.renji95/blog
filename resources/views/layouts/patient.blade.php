@extends('layouts.layout')

@section('nav-bar')
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" width="80px" height="80px" src="..\images\avatars\pikachu.jpg"/>
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong
                                        class="font-bold">Patient Name</strong>
                             </span> <span class="text-muted text-xs block">Patient Job<b
                                        class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="">Profile</a></li>
                            <li><a href="">Contacts</a></li>
                            <li><a href="">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('patient.logout')}}">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        CLINIC
                    </div>
                </li>
                <li>
                    <a href="{{ route('patient.index') }}"><span class="nav-label">Appointments</span></a>
                </li>
                <li>
                    <a href="{{ route('patient.get.appointments') }}"><span class="nav-label">Reserve</span></a>
                </li>
                <li>
                    <a href="{{ route('patient.history') }}"><span class="nav-label">History</span></a>
                </li>
                <li>
                    <a href="{{ route('patient.invoices') }}"><span class="nav-label">Invoices</span></a>
                </li>
            </ul>

        </div>
    </nav>
@endsection


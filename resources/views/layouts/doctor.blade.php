@extends('layouts.layout')

@section('nav-bar')
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" width="80px" height="80px" src="images\avatars\pikachu.jpg"/>
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong
                                            class="font-bold">Patient Name</strong>
                             </span> <span class="text-muted text-xs block">Doctor Job<b
                                            class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="">Profile</a></li>
                            <li><a href="">Contacts</a></li>
                            <li><a href="">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('doctor.logout')}}">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        CLINIC
                    </div>
                </li>
                <li>
                    <a href="{{route('doctor.index')}}"><i class="fa fa-calendar"></i><span class="nav-label">Appointments</span></a>
                </li>
                <li>
                    <a href="{{route('doctor.chat_room.view')}}"><i class="fa fa-comment"></i><span class="nav-label">Chat Room</span></a>
                </li>
            </ul>

        </div>
    </nav>
@endsection


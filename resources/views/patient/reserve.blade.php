@extends('layouts.patient')

@section('title', 'Reserve Page')

@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Book a new appointment</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li class="active">
                    <strong>Booking</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('ibox-title')
    <h5>Reserve a new appointment within one week back</h5>
    <div class="ibox-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
        <a class="close-link">
            <i class="fa fa-times"></i>
        </a>
    </div>
@endsection

@section('content')
    <form action="{{ route('patient.create.appointment') }}" method="post">
        {{ csrf_field() }}
        <div id="appointment">
            <div class="table-responsive">
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <label>Choose Shift</label>
                    <select class="form-control m-b" id="shift" name="shift">
                        <option selected value="shift">Shift</option>
                        @foreach($appointments['shifts'] as $shift)
                            <option name="shift" value="{{ $shift->id }}">From {{ $shift->startTime }}
                                to {{ $shift->endTime }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <label>Choose Date</label>
                    <select class="form-control m-b" id="date" name="date">
                        <option selected value="date">One week back</option>
                        @foreach($appointments['dates'] as $key => $date)
                            <option name="date">{{ $date }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2"></div>

                <div class="col-md-4">
                    <label>Choose your doctor</label>
                </div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Doctor's name</th>
                        <th>Email</th>
                        <th>Phone number</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($appointments['doctors'] as $doctor)
                        <tr class="gradeX">
                            <td>{{ $doctor->name }}</td>
                            <td>{{ $doctor->email }}</td>
                            <td>{{ $doctor->phone_number }}</td>
                            <td class="center">None</td>
                            <td class="center"><input type="radio" value="{{ $doctor->id }}" name="doctor"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div style="text-align:center;">
            <button class="btn btn-lg btn-primary m-t-n-xs" type="submit" style="top: 100%;" disabled id="btn-submit"><strong>Submit</strong>
            </button>
        </div>
    </form>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('change', "input[name='doctor']", function() {
                $.ajax({
                    url: "{{ route('patient.make.appointment') }}",
                    type: "post",
                    dataType: "json",
                    "data": {
                        doctorID: $("input[name='doctor']:checked").val(),
                        date: $('#date option:checked').val(),
                        shift: $('#shift option:checked').val(),
                        elementChange: 'doctorID',
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (result) {
                        $('#appointment').html(result);
                    }
                });
            });

            $(document).on('change', "#date", function() {
                $.ajax({
                    url: "{{ route('patient.make.appointment') }}",
                    type: "post",
                    dataType: "json",
                    "data": {
                        doctorID: $("input[name='doctor']:checked").val(),
                        date: $('#date option:checked').val(),
                        shift: $('#shift option:checked').val(),
                        elementChange: 'date',
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (result) {
                        $('#appointment').html(result);
                    }
                });
            });

            $(document).on('change', "#shift", function() {
                $.ajax({
                    url: "{{ route('patient.make.appointment') }}",
                    type: "post",
                    dataType: "json",
                    "data": {
                        doctorID: $("input[name='doctor']:checked").val(),
                        date: $('#date option:checked').val(),
                        shift: $('#shift option:checked').val(),
                        elementChange: 'shift',
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (result) {
                        $('#appointment').html(result);
                    }
                });
            });


            $(document).on('change', function () {
                let doctorID = $("input[name='doctor']:checked").val();
                let shift = $('#shift option:checked').val();
                let date = $('#date option:checked').val();
                if (doctorID != 'undefined' && shift != 'shift' && date != 'date') {
                    $("#btn-submit").prop('disabled', false);
                } else {
                    $("#btn-submit").prop('disabled', true);
                }
            });
        });
    </script>
@endsection


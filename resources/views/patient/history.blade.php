@extends('layouts.patient')

@section('title', 'Reserve Page')

@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Book a new appointment</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li class="active">
                    <strong>History</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('ibox-title')
    <h5>Your history</h5>
    <div class="ibox-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
        <a class="close-link">
            <i class="fa fa-times"></i>
        </a>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Doctor's name</th>
                <th>Date</th>
                <th>Shift</th>
                <th>Diagnostics</th>
                <th>Service used</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($histories as $history)
                <tr class="gradeX">
                    <td>{{ $history['doctorName'] }}</td>
                    <td>{{ $history['date'] }}</td>
                    <td>{{ $history['shift'] }}</td>
                    <td>{{ $history['diagnostic'] }}</td>
                    <td>
                        @foreach($history['services'] as $service)
                            <p>{{ $service->name }}</p>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

@endsection

@section('script')

@endsection


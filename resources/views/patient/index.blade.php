@extends('layouts.patient')

@section('title', 'Appointments')

@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Appointments are coming....</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li class="active">
                    <strong>Appointments</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('ibox-title')
    <h5>Appointments are coming....</h5>
    <div class="ibox-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
        <a class="close-link">
            <i class="fa fa-times"></i>
        </a>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Doctor's name</th>
                <th>Shift</th>
                <th>Date</th>
                {{--<th></th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($appointmentsBooked as $appointment)
                <tr class="gradeX">
                    <td>{{ $appointment['doctor'] }}</td>
                    <td>{{ $appointment['shift'] }}</td>
                    <td>{{ $appointment['date'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('script')

@endsection

@extends('layouts.patient')

@section('title', 'Reserve Page')

@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Book a new appointment</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li class="active">
                    <strong>Invoices</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('ibox-title')
    <h5>Your history</h5>
    <div class="ibox-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
        <a class="close-link">
            <i class="fa fa-times"></i>
        </a>
    </div>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th class="center">Payment Date</th>
                <th class="center">Amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $invoice)
                <tr class="gradeX">
                    <td>{{ $invoice->payment_date }}</td>
                    <td>{{ $history->amount }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

@endsection

@section('script')

@endsection


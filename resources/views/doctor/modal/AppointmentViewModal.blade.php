<div id="modal-form" class="modal inmodal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-body">
                <form method="POST" id="appointmentDetail_form">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-auto text-center">
                            <h1 class="m-t-none m-b">
                                <span class="text-navy"><b>{{$appointment->patient->name."'s Appointment"}}</b></span>
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <h3><span class="text-info"><b>Patient Infomation</b></span></h3>
                            <div class="space-15"></div>
                            <pre><b>Name   : </b>{{$appointment->patient->name}}
                                <br><b>Email  : </b>{{$appointment->patient->email}}
                                <br><b>Phone  : </b>{{$appointment->patient->phone_number}}
                                <br><b>Address: </b>{{$appointment->patient->address}}</pre>
                        </div>
                        <div class="form-group col-sm-offset-6">
                            <h3><span class="text-info"><b>Doctor Infomation</b></span></h3>
                            <div class="space-15"></div>
                            <pre><b>Name   : </b>{{$appointment->doctor->name}}
                                <br><b>Email  : </b>{{$appointment->doctor->email}}
                                <br><b>Phone  : </b>{{$appointment->doctor->phone_number}}
                                <br><b>Address: </b>{{$appointment->doctor->address}}</pre>
                        </div>
                    </div>
                    <div class="space-15"></div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <h3><span class="text-info"><b>Diagnostic</b></span></h3>
                            <div class="space-15"></div>
                            <textarea class="form-control" name="diagnostic" id="diagnostic"
                                      rows="14"
                                      required>{{($appointment->diagnostic)?$appointment->diagnostic['description']:''}}</textarea>
                        </div>
                        <div class="form-group col-sm-6">
                            <h3><span class="text-info"><b>Services</b></span></h3>
                            <div class="space-15"></div>
                            <div class="ibox">
                                <div class="ibox-content" id="service_form">
                                    {{--<form id="form" action="#" class="wizard-big">--}}
                                    <select class="form-control dual_select" id="services" multiple>
                                        @foreach($services as $service)
                                            @if($usedServices && in_array($service->id,$usedServices))
                                                <option value="{{$service->id}}"
                                                        selected="selected">{{$service->name}}</option>
                                            @else
                                                <option value="{{$service->id}}">{{$service->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    {{--</form>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12 text-right">
                            <span id="amount_show"></span>
                        </div>
                    </div>
                    <div class="row">
                        <span id="form_output"></span>
                    </div>
                    <div class="row" id="button-group">
                        @if($appointment->status_id != 4)
                            <div class="form-group col-sm-6">
                                <button class="btn btn-sm btn-primary" type="submit"><strong>Submit</strong></button>
                                <button class="btn btn-sm btn-primary" type="reset" onclick="resetModal()">
                                    <strong>Reset</strong></button>
                                <button class="btn btn-sm btn-primary" onclick="closeModal()" type="button">
                                    <strong>Close</strong></button>
                            </div>
                        @else
                            <div class="form-group col-sm-6">
                                <button class="btn btn-sm btn-primary" onclick="closeModal()" type="button">
                                    <strong>Close</strong></button>
                            </div>
                            @if($appointment->status_id == 4)
                                <div class="form-group col-sm-6 text-right">
                                    <a href="{{route('printInvoice',['appointment_id' => $appointment->id])}}" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-print"></i><strong>Print Invoice</strong>
                                    </a>
                                </div>
                            @endif
                        @endif
                        <div class="form-group col-sm-6 text-right" id="payment-button">
                            @if($appointment->status_id == 2)
                                <button class="btn btn-sm btn-primary" type="button" onclick="paymentSubmit(this.id)"
                                        id="{{$appointment->id}}"><strong>Payment confirmation
                                    </strong></button>
                            @endif
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/bootstrap.min.js"></script>
<script>

    function closeModal() {
        $('#modal-form').modal('toggle');
    }

    function paymentSubmit(id) {
        event.preventDefault(); // prevent form submit
        swal({
                title: "Are you sure?",
                text: "Please check all appointment's information",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, confirm!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('doctor.payment.submit') }}",
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            appointment_id: id
                        },
                        success: function (data) {
                            if (data.error.length > 0) {
                                swal('Cancelled', "Something is wrong!", "error");
                            }
                            else {
                                $('.dataTables-appointment').DataTable().ajax.reload();
                                $('#button-group').html('<div class="form-group col-sm-6">\n' +
                                    '                            <button class="btn btn-sm btn-primary" onclick="closeModal()" type="button">\n' +
                                    '                                <strong>Close</strong></button>\n' +
                                    '                        </div>\n' +
                                    '<div class="form-group col-sm-6 text-right">\n' +
                                    '<a href="{{route('printInvoice',['appointment_id' => $appointment->id])}}" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-print"></i><strong>Print Invoice</strong>\n' +
                                    '</a>\n' +
                                    '</div>'
                                );
                                swal({
                                        title: "Do you want to print bill?",
                                        type: "success",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Yes, Print!",
                                        cancelButtonText: "No, cancel please!",
                                        closeOnConfirm: true,
                                        closeOnCancel: false
                                    },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            window.open("{{route('printInvoice',['appointment_id' => $appointment->id])}}",'_blank');
                                        } else {
                                            swal("Cancelled", "no!!!", "error");
                                        }
                                    }
                                );


                            }
                        }
                    })
                } else {
                    swal("Cancelled", "Payment Confirm cancelled!!!", "error");
                }
            });
    }

    $('#services').on('change', function (event) {
        event.preventDefault();
        var services = $('#services').val();
        $.ajax({
            url: "{{ route('doctor.service.amount') }}",
            method: "POST",
            dataType: "json",
            data: {
                "_token": "<?= csrf_token() ?>",
                services: services
            },
            success: function (data) {
                $('#amount_show').html(data);
            }
        })
    })

    $('#modal-form').on('hidden.bs.modal', function (event) {
        $('body').css('padding-right', '0px');
    })

    $('#modal-form').on('show.bs.modal', function (event) {
        $('body').css('padding-right', '0px');
        var link = $(event.relatedTarget)
        $('#form_output').html('');
        $('#amount_show').html('<label class="text-success">Amount: {{number_format($amount)}}đ</label>');
    })

    $('.dual_select').bootstrapDualListbox({
        selectorMinimalHeight: 160
    });

    $('#appointmentDetail_form').on('submit', function (event) {
        $('#form_output').html('');
        var diagnostic = $('#diagnostic').val();
        var services = $('#services').val();
        console.log(services);
        event.preventDefault();
        $.ajax({
            url: "{{ route('doctor.appointment.submit') }}",
            method: "POST",
            dataType: "json",
            data: {
                _token: "{{csrf_token()}}",
                appointment_id: {{$appointment->id}}+'',
                diagnostic: diagnostic,
                services: services
            },
            success: function (data) {
                if (data.error.length > 0) {
                    $('#form_output').html(data.error);
                }
                else {
                    $('#form_output').html(data.success);
                    $('#payment-button').html('<button class="btn btn-sm btn-primary" type="button" id="{{$appointment->id}}" onclick="paymentSubmit(this.id)"><strong>Payment confirmation\n' +
                        '                                </strong></button>');
                    $('.dataTables-appointment').DataTable().ajax.reload();
                }
            }
        })

    });
</script>





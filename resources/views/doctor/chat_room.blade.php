@extends('layouts.doctor')

@section('title', 'Appointments')

@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Appointments are coming....</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li class="active">
                    <strong>Appointments</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('ibox-title')
    <h5>Doctor Chat Room</h5>
    <small class="pull-right text-muted">Last message: Mon Jan 26 2015 - 18:39:23</small>
@endsection

@section('content')
    <div class="ibox chat-view">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-9 ">
                    <div class="chat-discussion">

                        <div class="chat-message left">
                            <img class="message-avatar" src="images\avatars\pikachu.jpg" alt="">
                            <div class="message">
                                <a class="message-author" href="#"> Michael Smith </a>
                                <span class="message-date"> Mon Jan 26 2015 - 18:39:23 </span>
                                <span class="message-content">
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                            </span>
                            </div>
                        </div>
                        <div class="chat-message right">
                            <img class="message-avatar" src="images\avatars\pikachu.jpg" alt="">
                            <div class="message">
                                <a class="message-author" href="#"> Karl Jordan </a>
                                <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                                <span class="message-content">
											Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover.
                                            </span>
                            </div>
                        </div>
                        <div class="chat-message right">
                            <img class="message-avatar" src="images\avatars\pikachu.jpg" alt="">
                            <div class="message">
                                <a class="message-author" href="#"> Michael Smith </a>
                                <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                                <span class="message-content">
											There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.
                                            </span>
                            </div>
                        </div>
                        <div class="chat-message left">
                            <img class="message-avatar" src="images\avatars\pikachu.jpg" alt="">
                            <div class="message">
                                <a class="message-author" href="#"> Alice Jordan </a>
                                <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                                <span class="message-content">
											All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.
                                                It uses a dictionary of over 200 Latin words.
                                            </span>
                            </div>
                        </div>
                        <div class="chat-message right">
                            <img class="message-avatar" src="images\avatars\pikachu.jpg" alt="">
                            <div class="message">
                                <a class="message-author" href="#"> Mark Smith </a>
                                <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                                <span class="message-content">
											All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.
                                                It uses a dictionary of over 200 Latin words.
                                            </span>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-md-3">
                    <div class="chat-users">
                        <div class="users-list">
                            @foreach($doctors as $doctor)
                                <div class="chat-user">
                                    <img class="chat-avatar" src="images\avatars\pikachu.jpg" alt="">
                                    <div class="chat-user-name">
                                        <a href="#"><span class="text-success font-bold">{{$doctor->name}}</span></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="chat-message-form">

                        <div class="form-group">

                            <textarea class="form-control message-input" name="message"
                                      placeholder="Enter message text"></textarea>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>
@endsection
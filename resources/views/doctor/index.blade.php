@extends('layouts.doctor')

@section('title', 'Appointments')

@section('page-heading')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Appointments are coming....</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="">Home</a>
                </li>
                <li class="active">
                    <strong>Appointments</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('ibox-title')
    <h5>Appointments List</h5>
    <div class="ibox-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
        <a class="close-link">
            <i class="fa fa-times"></i>
        </a>
    </div>
@endsection

@section('content')
    <div class="form-group" id="data_5">
        {{ csrf_field() }}
        <div class="ibox-content row">
            <div class="col-sm-6">
                <label class="font-weight-bold">Hiển Thị theo ngày</label>
                <div class="input-daterange input-group" id="datepicker">
                    <span class="input-group-addon">Từ</span>
                    <input type="text" class="input-sm form-control" name="start"
                           value="{{$startDate}}"
                           id="startDate"/>
                    <span class="input-group-addon">đến</span>
                    <input type="text" class="input-sm form-control" name="end" value="{{$endDate}}"
                           id="endDate"/>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables-appointment"
               id="dataTables-appointment">
            <thead>
            <tr>
                <th data-priority="1">ID</th>
                <th data-priority="3">Patient</th>
                <th data-priority="4">Appointment</th>
                <th data-priority="5">Status</th>
                <th>Created</th>
                <th>Updated</th>
                <th data-priority="2">Action</th>
            </tr>
            </thead>
        </table>
    </div>
    <div id="AppointmentModalLoad"></div>
@endsection

@section('script')
    <script type="text/javascript" language="javascript">
        var start = document.getElementById('startDate').getAttribute('value');
        var end = document.getElementById('endDate').getAttribute('value');

        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
        $('#startDate').on('change', function () {
            start = this.value.trim();
            $('.dataTables-appointment').DataTable().ajax.reload();
        });
        $('#endDate').on('change', function () {
            end = this.value.trim();
            $('.dataTables-appointment').DataTable().ajax.reload();
        });

        function modalShow(id) {
            $.ajax({
                type: "POST",
                url: "{{route('doctor.AppointmentViewModalShow')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    id: id
                },
                success: function(data) {
                    $("#AppointmentModalLoad").html(data);
                    $("#modal-form").modal('toggle').target();
                }
            });
        }
        $(document).ready(function () {
            $('.dataTables-appointment').DataTable({
                "processing": true,
                "serverSide": true,
                "responsive": true,
                "stateSave": true,
                "stateDuration": -1,
                //"order": [[5, 'desc']],
                "ajax": {
                    "url": "<?= route('doctor.getListAppointment')?>",
                    "dataType": "json",
                    "type": "POST",
                    "data":
                        function (d) {
                            d.startDate = start,
                                d.endDate = end,
                                d._token = "<?= csrf_token() ?>"
                        },
                },
                "columns":
                    [
                        {
                            data: "id",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<span class='text-success'><b>" + oData.id + "</b><span>");
                            }
                        },
                        {
                            data: "patient_name", orderable: false,
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<span class='text-black-50'><b>" + oData.patient_name + "</b></span>");
                            }
                        },
                        {
                            data: "appointment", orderable: false,
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<span class='text-success'><b>" + oData.appointment + "</b></span>");
                            }
                        },
                        {
                            data: "status", orderable: false,
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                if(oData.status_id == 1){
                                    $(nTd).html("<span class='text-info'><b>" + oData.status + "</b></span>");
                                }else if(oData.status_id == 2){
                                    $(nTd).html("<span class='text-danger'><b>" + oData.status + "</b></span>");
                                }else if(oData.status_id == 4){
                                    $(nTd).html("<span class='text-success'><b>" + oData.status + "</b></span>");
                                }

                            }
                        },
                        {
                            data: "created_at"
                        },
                        {
                            data: "updated_at"
                        },
                        {
                            data: "id",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<center><a onclick='modalShow(this.id)' id='" + oData.id + "' class='btn btn-primary'><i class='fa fa-eye'>View</i></a></center>");
                            }, orderable: false
                        },

                    ],
                columnDefs: [
                    {className: 'control'},
                    {orderable: false},
                    {responsivePriority: 1, targets: 0},
                    {responsivePriority: 2, targets: -1},
                    {responsivePriority: 3, targets: 1},
                    {responsivePriority: 4, targets: 2},
                    {responsivePriority: 5, targets: 3},
                ],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });

    </script>
@endsection

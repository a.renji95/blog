<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Clinic | Invoice Print</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>

<body class="white-bg">
                <div class="wrapper wrapper-content p-xl">
                    <div class="ibox-content p-xl">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h5>From:</h5>
                                    <address>
                                        <strong>Clinic Hospital</strong><br>
                                        239 Xuan Thuy, Cau Giay, Ha Noi<br>
                                        5 and 6 floor, HITC Building<br>
                                        <abbr title="Phone">P:</abbr> (+84) 39 716 1671
                                    </address>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <h4>Invoice No.</h4>
                                    <h4 class="text-navy">{{$appointment->id}}</h4>
                                    <span>To:</span>
                                    <address>
                                        <strong>{{$appointment->patient->name}}</strong><br>
                                        {{$appointment->patient->email}}<br>
                                        {{$appointment->patient->address}}<br>
                                        <abbr title="Phone">P:</abbr> {{$appointment->patient->phone_number}}
                                    </address>
                                    <p>
                                        <span><strong>Invoice Date:</strong> {{$appointment->patient->created_at->modify('+7 hours')->format('H:m:i d/m/Y')}}</span><br/>
                                        <span><strong>Invoice Print Date:</strong> {{\Carbon\Carbon::now()->modify('+7 hours')->format('H:m:i d/m/Y')}}</span>
                                    </p>
                                </div>
                            </div>

                            <div class="table-responsive m-t">
                                <table class="table invoice-table">
                                    <thead>
                                    <tr>
                                        <th class="h3">Service List</th>
                                        <th class="h3">Unit Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($appointment->services)
                                        @foreach($appointment->services as $service)
                                        <tr>
                                            <td><div><strong>{{$service->name}}</strong></div></td>
                                            <td>{{number_format($service->price)}}đ</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /table-responsive -->

                            <table class="table invoice-total">
                                <tbody>
                                <tr>
                                    <td><strong>TOTAL :</strong></td>
                                    <td>{{number_format($appointment->invoice->amount)}}đ</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

    </div>

    <!-- Mainly scripts -->
                <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
                <script src="{{ asset('js/bootstrap.min.js') }}"></script>
                <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <!-- Custom and plugin javascript -->
                <script src="{{ asset('js/inspinia.js') }}"></script>

    <script type="text/javascript">
        window.print();
    </script>

</body>

</html>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clinic</title>
    <base href="{{asset('')}}">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/flashMessage.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</head>
<body class="gray-bg">
@if(Session::has('success'))
    <script>
        swal({
                title: "Register Successful",
                text: "Do you wanna go home page?",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, let's go!",
                cancelButtonText: "No, stay here!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = "{{route('patient.home')}}";
                } else {
                    {{\Illuminate\Support\Facades\Auth::logout()}}
                    swal("Cancelled", "Stay Register Page");
                }
            });
    </script>
@endif
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div id="main">
        <div>

            <h1 class="logo-name">IN+</h1>

        </div>
        <h3>Register to IN+</h3>
        <p>Create account to see it in action.</p>
        <div class="text-error">
            <form class="m-t" role="form" method="POST" enctype="multipart/form-data" action="{{route('patient.register.submit')}}"
                  id="registerForm">
                {{ csrf_field() }}
                <div class="form-group">
                    <input name="name" id="name" type="text" class="form-control" placeholder="Name">
                </div>
                <div class="form-group">
                    <input name="email" id="email" type="email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                    <input name="password" id="password" type="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                    <input name="password_confirmation" id="password_confirmation" type="password" class="form-control"
                           placeholder="Password Confirm">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Register</button>
                <div class="form-group">
                    @include('layouts.errors')
                    {{--@include('layouts.success')--}}
                </div>
                <p class="text-muted text-center">
                    <small>Already have an account?</small>
                </p>
                <a class="btn btn-sm btn-white btn-block" href="{{route('login')}}">Login</a>
            </form>
        </div>
        <p class="m-t">
            <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
        </p>
    </div>
</div>
{{--<script src="js/bootstrap.min.js"></script>--}}
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">

    $().ready(function () {
        $("#registerForm").validate({
            rules: {
                "name": {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                "email": {
                    required: true,
                    validateEmail: true,
                    maxlength: 50
                },
                "password": {
                    required: true,
                    minlength: 10,
                    maxlength: 20,
                    validatePassword: true
                },
                "password_confirmation": {
                    required: true,
                    equalTo: "#password"
                }
            },
            messages: {
                "name": {
                    required: 'Name cant empty',
                    minlength: 'Name must contains 6 to 20 character',
                    maxlength: 'Name must contains 6 to 20 character'
                },
                "email": {
                    required: 'Email required',
                    email: 'Invalid Email',
                    maxlength: 'Invalid Email'
                },
                "password": {
                    required: 'Password required',
                    minlength: 'Password must contain at least 10 characters',
                    maxlength: 'Password must be less than 20 characters'
                },
                "password_confirmation": {
                    required: 'Password confirm required',
                    equalTo: "Password confirm not match"
                }
            }
        });
        $.validator.addMethod("validateEmail", function (value, element) {
            return this.optional(element) || /^[a-zA-Z]+([a-zA-Z0-9_.])*@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/.test(value);
        }, "Invalid Email");
        $.validator.addMethod("validatePassword", function (value, element) {
            return this.optional(element) || /^(?!.*[\s])(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@$#%^&*]).+$/.test(value);
        }, "Password must contain at least one uppercase letter, one lowercase letter, one number, one special character and cant contain space");
    });

</script>
</body>

</html>
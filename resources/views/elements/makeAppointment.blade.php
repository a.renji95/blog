<div class="table-responsive">
    <div class="col-md-1"></div>
    <div class="col-md-3">
        <label>Choose Shift</label>
        <select class="form-control m-b" id="shift" name="shift">
            <option selected value="shift">Shift</option>
            @foreach($appointments['shifts'] as $shift)
                <option name="shift" value="{{ $shift->id }}">From {{ $shift->startTime }}
                    to {{ $shift->endTime }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-3">
        <label>Choose Date</label>
        <select class="form-control m-b" id="date" name="date">
            <option selected value="date">One week back</option>
            @foreach($appointments['dates'] as $key => $date)
                <option name="date">{{ $date }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-2"></div>

    <div class="col-md-4">
        <label>Choose your doctor</label>
    </div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Doctor's name</th>
            <th>Email</th>
            <th>Phone number</th>
            <th>Status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($appointments['doctors'] as $doctor)
            <tr class="gradeX">
                <td>{{ $doctor->name }}</td>
                <td>{{ $doctor->email }}</td>
                <td>{{ $doctor->phone_number }}</td>
                <td class="center">None</td>
                <td class="center"><input type="radio" value="{{ $doctor->id }}" name="doctor"></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <input type="hidden" value="{{ $shiftBooked }}" name="shiftBooked" id="shiftBooked">
    <input type="hidden" value="{{ $dateBooked }}" name="dateBooked" id="dateBooked">
    <input type="hidden" value="{{ $doctorBooked }}" name="doctorBooked" id="doctorBooked">
</div>

<script>
    let shiftBooked = $('#shiftBooked').val();
    let dateBooked = $('#dateBooked').val();
    let doctorBooked = $('#doctorBooked').val();

    $('#shift').val(shiftBooked);
    $('#date').val(dateBooked);
    $("input[name='doctor'][value=" + doctorBooked + "]").attr('checked', 'checked');
</script>

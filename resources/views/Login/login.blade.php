<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clinic</title>
    <base href="{{asset('')}}">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">

<div class="loginColumns animated fadeInDown">
    <div class="row">

        <div class="col-md-6">
            <h2 class="font-bold">Welcome to Clinic</h2>

            <p>
                Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
            </p>

            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s.
            </p>

            <p>
                When an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>

            <p>
                <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                    essentially unchanged.
                </small>
            </p>

        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" role="form" method="POST" enctype="multipart/form-data" action="{{route('patient.login.submit')}}" id="loginForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input name="email" type="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                    <a href="#">
                        <small>Forgot password?</small>
                    </a>

                    <p class="text-muted text-center">
                        <small>Do not have an account?</small>
                    </p>
                    <a class="btn btn-sm btn-white btn-block" href="{{route('patient.register')}}">Create an account</a>
                </form>
                <div class="space-15"></div>
                <div class="form-group">
                    @include('layouts.errors')
                </div>
                <p class="m-t">
                    <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                </p>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Copyright Example Company
        </div>
        <div class="col-md-6 text-right">
            <small>© 2014-2015</small>
        </div>
    </div>
</div>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $().ready(function () {
        $("#loginForm").validate({
            rules: {
                "email": {
                    required: true,
                    validateEmail: true,
                    maxlength: 50
                },
                "password": {
                    required: true
                }
            },
            messages: {
                "email": {
                    required: 'Email required',
                    email: 'Invalid Email',
                    maxlength: 'Invalid Email'
                },
                "password": {
                    required: 'Password required'
                }
            }
        });
        $.validator.addMethod("validateEmail", function (value, element) {
            return this.optional(element) || /^[a-zA-Z]+([a-zA-Z0-9_.])*@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/.test(value);
        }, "Invalid Email");
    });
</script>
</body>

</html>

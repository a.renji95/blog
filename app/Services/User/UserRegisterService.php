<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/19/2018
 * Time: 1:50 PM
 */

namespace App\Services\User;

use App\Models\User;
use App\Repositories\User\UserEloquentRepository;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class UserRegisterService
{

    protected $userEloquentRepository;

    public function __construct(
        UserEloquentRepository $userEloquentRepository
    )
    {
        $this->userEloquentRepository = $userEloquentRepository;
    }

    public function register($requestParams)
    {
        $user = new User();
        $user->name = $requestParams['name'];
        $user->email = $requestParams['email'];
        $user->password = $requestParams['password'];
        try{
            $result = $this->userEloquentRepository->insert($user);
        }catch (Exception $e){
            $result = false;
        }
        return $result;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/19/2018
 * Time: 11:34 AM
 */
namespace App\Services\Validators;


use App\Services\ReadFiles\Validators\validateMessage;

class ValidatorService{

    protected $validateMessage;

    public function __construct(validateMessage $validateMessage)
    {
        $this->validateMessage = $validateMessage;
    }

    public function registerValidate($th, $request)
    {
        $nameMessage = $this->validateMessage->readValidateMessageFile('app\validateMessages\register\validateName');
        $emailMessage = $this->validateMessage->readValidateMessageFile('app\validateMessages\register\validateEmail');
        $passwordMessage = $this->validateMessage->readValidateMessageFile('app\validateMessages\register\validatePassword');
        $password_confirmationMessage = $this->validateMessage->readValidateMessageFile('app\validateMessages\register\validatePasswordConfirmation');
        $th->validate($request, [
            'name' => 'required|between:6,20',
            'email' => 'required|regex:/^[a-zA-Z]+([a-zA-Z0-9_.])*@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/|max:50|unique:Users',
            'password' => 'required|confirmed|min:10|max:20|regex:/^(?!.*[\s])(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@$#%^&*]).+$/',
            'password_confirmation' => 'required'
        ],
            [
                'name.'.$nameMessage[0][0] => $nameMessage[0][1],
                'name.'.$nameMessage[1][0] => $nameMessage[1][1],
                'email.'.$emailMessage[0][0] => $emailMessage[0][1],
                'email.'.$emailMessage[1][0] => $emailMessage[1][1],
                'email.'.$emailMessage[2][0] => $emailMessage[2][1],
                'email.'.$emailMessage[3][0] => $emailMessage[3][1],
                'password.'.$passwordMessage[0][0] => $passwordMessage[0][1],
                'password.'.$passwordMessage[1][0] => $passwordMessage[1][1],
                'password.'.$passwordMessage[2][0] => $passwordMessage[2][1],
                'password.'.$passwordMessage[3][0] => $passwordMessage[3][1],
                'password.'.$passwordMessage[4][0] => $passwordMessage[4][1],
                'password_confirmation.'.$password_confirmationMessage[0][0] => $password_confirmationMessage[0][1]
            ]
        );
    }



}

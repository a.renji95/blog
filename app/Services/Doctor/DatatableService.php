<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/16/2018
 * Time: 2:14 PM
 */

namespace App\Services\Doctor;

use App\Repositories\Appointment\AppointmentEloquentRepository;
use App\Repositories\DatatableRequestRepositoryInterface;
use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class DatatableService
{

    protected $datatableRequestRepositoty;
    protected $appointmentEloquentRepository;

    public function __construct(
        DatatableRequestRepositoryInterface $datatableRequestRepository,
        AppointmentEloquentRepository $appointmentEloquentRepository
    )
    {
        $this->datatableRequestRepositoty = $datatableRequestRepository;
        $this->appointmentEloquentRepository = $appointmentEloquentRepository;
    }

    public function getAppointmentListAjax($data, $columns)
    {
        $search = $data['search'];
        if (empty($search)) {
            $posts = Appointment::where('doctor_id', Auth::user()->id)->whereBetween('date',[$data['startDate'],$data['endDate']])->offset($data['start'])->limit($data['length'])->orderBy($columns[$data['oderColunm']], $data['oderSortType'])->get();
        } else {
            $posts = Appointment::where('doctor_id', Auth::user()->id)
                ->whereBetween('date',[$data['startDate'],$data['endDate']])
                ->where(function ($query) use ($search) {
                    $query->where('id', 'like', "%$search%");
                    $query->orWhereHas('patient', function ($query) use ($search) {
                        $query->where('name', 'like', "%$search%");
                    });
                    $query->orWhereHas('status', function ($query) use ($search) {
                        $query->where('name', 'like', "%$search%");
                    });
                })
                ->offset($data['start'])->limit($data['length'])->orderBy($columns[$data['oderColunm']], $data['oderSortType'])->get();
        }
        return $posts;
    }

    public function getNumberRecordOfAppointmentBySearch($search)
    {
        $numberOfRecord = Appointment::where('doctor_id', Auth::user()->id)
            ->where(function ($query) use ($search) {
                $query->where('id', 'like', "%$search%");
                $query->orWhereHas('patient', function ($query1) use ($search) {
                    $query1->where('name', 'like', "%$search%");
                });
                $query->orWhereHas('status', function ($query) use ($search) {
                    $query->where('name', 'like', "%$search%");
                });
            })
            ->count();
        return $numberOfRecord;
    }

    public function getAppointment($request)
    {
        $data = $this->datatableRequestRepositoty->getDefaultRequest($request);
        $startDate = trim($request->input('startDate'))?: Carbon::now()->format('m-d-Y');
        $endDate = trim($request->input('endDate'))?: Carbon::now()->format('m-d-Y');
        $startDate = substr($startDate, -4) . '-' . substr($startDate, 0, 2) .'-'. substr($startDate, 3, -5);
        $endDate = substr($endDate, -4) . '-' . substr($endDate, 0, 2) .'-'. substr($endDate, 3, -5);
        $data['startDate'] = $startDate;
        $data['endDate'] = $endDate;
        $columns = array(
            0 => 'id',
            4 => 'created_at',
            5 => 'updated_at'
        );
        $totalData = $this->appointmentEloquentRepository->numberRecordOfAppointmentByDoctorID(Auth::user()->id);
        $appointments = $this->getAppointmentListAjax($data, $columns);
        $totalFiltered = $this->getNumberRecordOfAppointmentBySearch($data['search']);
        if (empty($data['search'])) {
            $totalFiltered = $totalData;
        }
        $dataArr = array();
        if ($appointments) {
            foreach ($appointments as $appointment) {
                $nestedData = array();
                $nestedData['id'] = $appointment->id;
                $nestedData['patient_name'] = $appointment->patient->name;
                $nestedData['patient_id'] = $appointment->patient->id;
                $nestedData['status'] = $appointment->status->name;
                $nestedData['status_id'] = $appointment->status->id;
                $startTime = Carbon::parse($appointment->shift->startTime)->format('H:i');
                $endTime = Carbon::parse($appointment->shift->endTime)->format('H:i');
                $date = Carbon::parse($appointment->date)->format('d/m/Y');
                $nestedData['appointment'] = $startTime . '-' . $endTime . ' ' . $date;
                $nestedData['created_at'] = $appointment->created_at->modify('+7 hours')->format('H:i:s d/m/Y');
                $nestedData['updated_at'] = $appointment->updated_at->modify('+7 hours')->format('H:i:s d/m/Y');
                $dataArr[] = $nestedData;

            }
        }
        $json_data = array(
            "draw" => intval($data['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $dataArr,
        );
        return $json_data;
    }

}

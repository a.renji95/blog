<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 10/19/2018
 * Time: 11:34 AM
 */
namespace App\Services\Doctor;

use App\Repositories\Appointment\AppointmentEloquentRepository;
use App\Repositories\Diagnostic\DiagnosticEloquentRepository;
use App\Repositories\AppointmentService\AppointmentServiceEloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\Invoices\InvoiceEloquentRepository;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AppointmentService{

    protected $appointmentEloquentRepository;
    protected $diagnosticEloquentRepository;
    protected $appointmentServiceEloquentRepository;
    protected $invoiceEloquentRepository;

    public function __construct(
        AppointmentEloquentRepository $appointmentEloquentRepository,
        DiagnosticEloquentRepository $diagnosticEloquentRepository,
        AppointmentServiceEloquentRepository $appointmentServiceEloquentRepository,
        InvoiceEloquentRepository $invoiceEloquentRepository
    )
    {
        $this->appointmentEloquentRepository = $appointmentEloquentRepository;
        $this->diagnosticEloquentRepository = $diagnosticEloquentRepository;
        $this->appointmentServiceEloquentRepository = $appointmentServiceEloquentRepository;
        $this->invoiceEloquentRepository = $invoiceEloquentRepository;
    }

    public function addDiagnosticService($request){
        $appointment_id = trim($request['appointment_id']);
        $diagnostic = trim($request['diagnostic']);
        $services = $request['services'];
        $error = '';
        $success_output = '';
        try {
            DB::beginTransaction();
            $data = array();
            $data['status_id'] = 2;
            $this->appointmentEloquentRepository->update($appointment_id, $data);
            $data = array();
            $data['appointment_id'] = $appointment_id;
            $data['description'] = $diagnostic;
            $diagnosticObject = $this->diagnosticEloquentRepository->findDiagnosticByAppointmentId($appointment_id);
            if($diagnosticObject){
                $this->diagnosticEloquentRepository->update($diagnosticObject->id, $data);
            }else{
                $this->diagnosticEloquentRepository->create($data);
            }
            $this->appointmentServiceEloquentRepository->removeAppointmentServiceByAppointmentId($appointment_id);
            if($services){
                foreach ($services as $service){
                    $data = array();
                    $data['appointment_id'] = $appointment_id;
                    $data['service_id'] = $service;
                    $this->appointmentServiceEloquentRepository->create($data);
                }
            }
            $success_output = '<div class="alert alert-success">Appointment update successful!!</div>';
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $error = '<div class="alert alert-danger">Something wrong!! Please try again!!</div>';
        }
        $output = array(
            'error' => $error,
            'success' => $success_output
        );
        return $output;
    }

    public function paymentConfirm($dataParam){
        $error = '';
        $success_output = '';
        try {
            DB::beginTransaction();
            $appointment = $this->appointmentEloquentRepository->find($dataParam['appointment_id']);
            $data = array();
            $data['status_id'] = 4;
            $this->appointmentEloquentRepository->update($appointment->id,$data);
            $amount = 0;
            if($appointment->services){
                foreach ($appointment->services as $service){
                    $amount += $service->price;
                }
            }
            $data = array();
            $data['status_id'] = 1;
            $data['patient_id'] = $appointment->patient_id;
            $data['appointment_id'] = $appointment->id;
            $data['amount'] = $amount;
            $data['payment_date'] = Carbon::now()->modify('+7 hours')->format('Y-m-d H:i:s');
            $this->invoiceEloquentRepository->create($data);

            $success_output = '<div class="alert alert-success">Payment Confirm Successful</div>';
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $error = '<div class="alert alert-danger">Something wrong!!!</div>';
        }
        $output = array(
            'error' => $error,
            'success' => $success_output
        );
        return $output;
    }
}

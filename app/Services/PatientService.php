<?php

namespace App\Services;

use App\Repositories\Contracts\DoctorRepositoryInterface;
use App\Repositories\Contracts\PatientRepositoryInterface;
use App\Repositories\Contracts\ShiftRepositoryInterface;
use App\Repositories\Eloquent\AvailableAppointmentRepository;
use Carbon\Carbon;

class PatientService extends BaseService
{
    protected $patientRepository;
    protected $doctorRepository;
    protected $shiftRepository;
    protected $availableAppointmentRepository;

    public function __construct(
        PatientRepositoryInterface $patientRepository,
        DoctorRepositoryInterface $doctorRepository,
        ShiftRepositoryInterface $shiftRepository,
        AvailableAppointmentRepository $availableAppointmentRepository
    )
    {
        $this->patientRepository = $patientRepository;
        $this->doctorRepository = $doctorRepository;
        $this->shiftRepository = $shiftRepository;
        $this->availableAppointmentRepository = $availableAppointmentRepository;
    }

    public function getAppointments()
    {
        $dateRange = 7;
        // Update Available Appointment
        $allDoctorsID = $this->doctorRepository->getListDoctorsID();
        $allShiftsID = $this->shiftRepository->getListShiftsID();
        $this->availableAppointmentRepository->updateAvailableAppointment($allShiftsID, $allDoctorsID, $dateRange);
        $appointments = [];
        $availableDoctorsID = $this->availableAppointmentRepository->getListAvailableDoctors();
        $appointments['doctors'] = $this->doctorRepository->getListDoctorsAvailable($availableDoctorsID);
        $appointments['shifts'] = $this->shiftRepository->getListShifts();
        $dates = [];
        for ($i = 0; $i < $dateRange; $i ++) {
            $dates[$i] = Carbon::now()->copy()->addDays($i)->format('Y-m-d');
        }
        $appointments['dates'] = $dates;
        return $appointments;
    }

}

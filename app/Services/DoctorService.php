<?php

namespace App\Services;

use App\Repositories\Contracts\DoctorRepositoryInterface;

class DoctorService extends BaseService
{
    protected $doctorRepository;

    public function __construct(DoctorRepositoryInterface $doctorRepository)
    {
        $this->doctorRepository = $doctorRepository;
    }

}

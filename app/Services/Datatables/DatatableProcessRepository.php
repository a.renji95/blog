<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/16/2018
 * Time: 2:05 PM
 */
namespace App\Services\Datatables;
use App\Repositories\DatatableProcessRepositoryInterface;
use App\Services\Doctor\DatatableService;
use Illuminate\Support\Facades\Log;

class DatatableProcessRepository implements DatatableProcessRepositoryInterface {

    protected $datatableService;

    public function __construct(DatatableService $datatableService)
    {
        $this->datatableService = $datatableService;
    }

    public function fillAppointmentTable($request)
    {
        try{
            $output = $this->datatableService->getAppointment($request);
        }catch(\Exception $ex){
            $output = array(
                "draw"            => intval(0),
                "recordsTotal"    => intval(0),
                "recordsFiltered" => intval(0),
                "data"            => array(($request->input('start') ?: 0).",".($request->input('length') ?: 10).",".($request->input('search.value') ?: "").",".($request->input('order.0.column') ?: 0).",".($request->input('order.0.dir') ?: 'desc').",".($request->draw ?: 0)),
            );
        }
        return $output;
    }
}

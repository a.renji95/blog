<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/16/2018
 * Time: 1:34 PM
 */
namespace App\Services\Datatables;

use App\Repositories\DatatableRequestRepositoryInterface;
use Illuminate\Support\Facades\Log;

class DatatableRequestRepository implements DatatableRequestRepositoryInterface {

    public function getDefaultRequest($request)
    {
        $start = $request->input('start') ?: 0;
        $length = $request->input('length') ?: 10;
        $search = $request->input('search.value') ?: "";
        $oderColunm = $request->input('order.0.column') ?: 0;
        $oderSortType = $request->input('order.0.dir') ?: 'desc';
        $draw = $request->draw ?: 0;
        $data = array();
        $data['start'] = $start;
        $data['length'] = $length;
        $data['search'] = $search;
        $data['oderColunm'] = $oderColunm;
        $data['oderSortType'] = $oderSortType;
        $data['draw'] = $draw;
        return $data;
    }
}


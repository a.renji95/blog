<?php

namespace App\Services;

use App\Models\Shift;
use App\Models\Status;
use App\Repositories\Contracts\AppointmentRepositoryInterface;
use App\Repositories\Contracts\AvailableAppointmentRepositoryInterface;
use App\Models\User;
use App\Repositories\Contracts\DoctorRepositoryInterface;
use App\Repositories\Contracts\InvoicesRepositoryInterface;
use App\Repositories\Contracts\ShiftRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class AppointmentService extends BaseService
{
    protected $availableAppointmentRepository;
    protected $appointmentRepository;
    protected $shiftRepository;
    protected $doctorRepository;
    protected $invoiceRepository;

    public function __construct(
        AvailableAppointmentRepositoryInterface $availableAppointmentRepository,
        AppointmentRepositoryInterface $appointmentRepository,
        ShiftRepositoryInterface $shiftRepository,
        DoctorRepositoryInterface $doctorRepository,
        InvoicesRepositoryInterface $invoiceRepository
    )
    {
        $this->availableAppointmentRepository = $availableAppointmentRepository;
        $this->appointmentRepository = $appointmentRepository;
        $this->shiftRepository = $shiftRepository;
        $this->doctorRepository = $doctorRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function getAppointmentsBooked(){
        $appointments = $this->appointmentRepository->getAppointmentsBooked();
        $appointmentsBooked = [];
        foreach ($appointments as $appointment) {
            $appointmentBooked = [];
            $shift =  $this->shiftRepository->getShift($appointment->shift_id);
            $appointmentBooked['shift'] = $shift->startTime . ' : ' . $shift->endTime;
            $appointmentBooked['date'] = $appointment->date;
            $doctor = $this->doctorRepository->getDoctor($appointment->doctor_id);
            $appointmentBooked['doctor'] = $doctor->name;
            $appointmentsBooked[] = $appointmentBooked;
        }

        return $appointmentsBooked;
    }

    public function getAvailableAppointmentByDoctorID($data)
    {
        $result = [];
        $appointments = $this->availableAppointmentRepository->getAppointmentByDoctorID($data);
        $availableAppointments = $appointments->where('status_id', Status::STATUS_AVAILABLE);

        $result['shiftBooked'] = $data['shift'];
        $result['dateBooked'] = $data['date'];

        if ($data['shift'] != 'shift' && $data['date'] == 'date') {
            $availableAppointments = $availableAppointments->where('shift_id', $data['shift']);
        }
        if ($data['shift'] == 'shift' && $data['date'] != 'date') {
            $availableAppointments = $availableAppointments->where('date', $data['date']);
        }
        if ($data['shift'] != 'shift' && $data['date'] != 'date') {
            $availableAppointments = $availableAppointments->where('date', $data['date'])->where('shift_id', $data['shift']);
        }

        $availableShiftsID = $availableAppointments->unique('shift_id')->pluck('shift_id');
        $result['availableShifts'] = Shift::whereIn('id', $availableShiftsID)->get();
        if ($availableShiftsID->isEmpty()) {
            $result['shiftBooked'] = 'shift';
            $availableShiftsID = $appointments->where('status_id', Status::STATUS_AVAILABLE)->unique('shift_id')->pluck('shift_id');
            $result['availableShifts'] = Shift::whereIn('id', $availableShiftsID)->get();
        }

        $result['availableDates'] = $availableAppointments->unique('date')->pluck('date');
        if ($result['availableDates']->isEmpty()) {
            $result['dateBooked'] = 'date';
            $result['availableDates'] = $appointments->where('status_id', Status::STATUS_AVAILABLE)->unique('date')->pluck('date');
        }

        $availableDoctorsID = $this->availableAppointmentRepository->getListAvailableDoctors();
        $result['availableDoctors'] = User::whereIn('id', $availableDoctorsID)->get();

        return $result;
    }

    public function getAvailableAppointmentByDate($data)
    {
        $result = [];
        $appointments = $this->availableAppointmentRepository->getAppointmentByDate($data);
        $availableAppointments = $appointments->where('status_id', Status::STATUS_AVAILABLE);

        $result['shiftBooked'] = $data['shift'];
        if (isset($data['doctorID'])) {
            $result['doctorBooked'] = $data['doctorID'];
        } else {
            $result['doctorBooked'] = 'doctor';
        }

        if ($data['shift'] != 'shift' && !isset($data['doctorID'])) {
            $availableAppointments = $availableAppointments->where('shift_id', $data['shift']);
        }
        if ($data['shift'] == 'shift' && isset($data['doctorID'])) {
            $availableAppointments = $availableAppointments->where('doctor_id', $data['doctorID']);
        }
        if ($data['shift'] != 'shift' && isset($data['doctorID'])) {
            $availableAppointments = $availableAppointments->where('doctor_id', $data['doctorID'])->where('shift_id', $data['shift']);
        }

        $availableDoctorsID = $this->availableAppointmentRepository->getListAvailableDoctors();
        $result['availableDoctors'] = User::whereIn('id', $availableDoctorsID)->get();

        $availableShiftsID = $availableAppointments->unique('shift_id')->pluck('shift_id');
        $result['availableShifts'] = Shift::whereIn('id', $availableShiftsID)->get();
        if ($availableShiftsID->isEmpty()) {
            $result['shiftBooked'] = 'shift';
            $availableShiftsID = $appointments->where('status_id', Status::STATUS_AVAILABLE)->unique('shift_id')->pluck('shift_id');
            $result['availableShifts'] = Shift::whereIn('id', $availableShiftsID)->get();
        }

        $result['availableDates'] = $this->availableAppointmentRepository->getListAvailableDates();

        return $result;
    }

    public function getAvailableAppointmentByShift($data)
    {
        $result = [];
        $appointments = $this->availableAppointmentRepository->getAppointmentByShift($data);
        $availableAppointments = $appointments->where('status_id', Status::STATUS_AVAILABLE);
        $result['dateBooked'] = $data['date'];
        if (isset($data['doctorID'])) {
            $result['doctorBooked'] = $data['doctorID'];
        } else {
            $result['doctorBooked'] = 'doctor';
        }

        if ($data['date'] != 'date' && !isset($data['doctorID'])) {
            $availableAppointments = $availableAppointments->where('date', $data['date']);
        }
        if ($data['date'] == 'date' && isset($data['doctorID'])) {
            $availableAppointments = $availableAppointments->where('doctor_id', $data['doctorID']);
        }
        if ($data['date'] != 'date' && isset($data['doctorID'])) {
            $availableAppointments = $availableAppointments->where('doctor_id', $data['doctorID'])->where('date', $data['date']);
        }

        $availableDoctorsID = $this->availableAppointmentRepository->getListAvailableDoctors();
        $result['availableDoctors'] = User::whereIn('id', $availableDoctorsID)->get();

        $availableShiftsID = $this->availableAppointmentRepository->getListAvailableShifts();
        $result['availableShifts'] = Shift::whereIn('id', $availableShiftsID)->get();

        $result['availableDates'] = $availableAppointments->unique('date')->pluck('date');
        if ($result['availableDates']->isEmpty()) {
            $result['dateBooked'] = 'date';
            $result['availableDates'] = $appointments->where('status_id', Status::STATUS_AVAILABLE)->unique('date')->pluck('date');
        }

        return $result;
    }

    public function createAppointment($data)
    {
        $this->appointmentRepository->createAppointment($data);
        $this->availableAppointmentRepository->makeUnavailableAppointment($data);
        return true;
    }

    public function getHistoryAppointments()
    {
        $histories = [];
        $patient_id = Auth::id();
        $appointments = $this->appointmentRepository->getHistoryAppointments($patient_id);
        foreach ($appointments as $appointment) {
            $history = [];
            $history['doctorName'] = $appointment->doctor->name;
            $history['date'] = $appointment->date;
            $history['shift'] = 'From ' . $appointment->shift->startTime . ' To ' . $appointment->shift->endTime;
            $history['diagnostic'] = $appointment->diagnostic['description'];
            $history['services'] = $appointment->services;
            $histories[] = $history;
        }
        return $histories;
    }

    public function getInvoices()
    {
        $patient_id = Auth::id();
        return $this->invoiceRepository->getInvoices($patient_id);
    }
}

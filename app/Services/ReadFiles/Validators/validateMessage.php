<?php
/**
 * Created by PhpStorm.
 * User: HiepNH
 * Date: 11/2/2018
 * Time: 10:32 AM
 */

namespace App\Services\ReadFiles\Validators;
use Illuminate\Support\Facades\File;


class validateMessage
{

    public function readValidateMessageFile($path){
        $array = array();
        $file = File::get(storage_path($path));
        //$file = File::get(".\storage".$path);
        foreach (explode("\n", $file) as $key=>$line){
            if(!empty(trim($line))){
                $array[$key] = explode('|', $line);
            }
        }
        return $array;
    }
}

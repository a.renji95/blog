<?php

namespace App\Services;

use App\Repositories\Contracts\ShiftRepositoryInterface;

class ShiftService extends BaseService
{
    protected $shiftRepository;

    public function __construct(ShiftRepositoryInterface $shiftRepository)
    {
        $this->shiftRepository = $shiftRepository;
    }
}

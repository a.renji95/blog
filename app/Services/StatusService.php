<?php

namespace App\Services;

use App\Repositories\Contracts\StatusRepositoryInterface;

class StatusService extends BaseService
{
    protected $statusRepository;

    public function __construct(
        StatusRepositoryInterface $statusRepository
    )
    {
        $this->statusRepository = $statusRepository;
    }
}

<?php

namespace App\Repositories\Contracts;

interface ShiftRepositoryInterface extends BaseRepositoryInterface
{
    public function getListShifts();
    public function getShift($id);
    public function getListShiftsID();
}

<?php

namespace App\Repositories\Contracts;

interface InvoicesRepositoryInterface extends BaseRepositoryInterface
{
    public function getInvoices($patient_id);
}

<?php

namespace App\Repositories\Contracts;

interface AvailableAppointmentRepositoryInterface extends BaseRepositoryInterface
{
    public function getAppointmentByDoctorID($data);
    public function getAppointmentByDate($data);
    public function getAppointmentByShift($data);
    public function getListAvailableDoctors();
    public function getListAvailableDates();
    public function getListAvailableShifts();
    public function makeUnavailableAppointment($data);
    public function updateAvailableAppointment($allShiftsID, $allDoctorsID, $dateRange);
}

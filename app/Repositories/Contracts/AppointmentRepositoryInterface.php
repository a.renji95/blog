<?php

namespace App\Repositories\Contracts;

interface AppointmentRepositoryInterface extends BaseRepositoryInterface
{
    public function createAppointment($data);
    public function getAppointmentsBooked();
    public function getHistoryAppointments($patient_id);
}

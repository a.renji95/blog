<?php

namespace App\Repositories\Contracts;

interface DoctorRepositoryInterface extends BaseRepositoryInterface
{
    public function getListDoctorsAvailable($availableDoctorsID);
    public function getDoctor($id);
    public function getListDoctorsID();
}

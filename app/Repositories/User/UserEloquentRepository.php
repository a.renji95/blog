<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/9/2018
 * Time: 10:29 AM
 */

namespace App\Repositories\User;

use App\Repositories\EloquentRepository;
use App\Models\User;


class UserEloquentRepository extends EloquentRepository implements UserRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return User::class;
    }

    public function getAll()
    {
        return parent::getAll();
    }



    public function create(array $attributes)
    {
        return parent::create($attributes);
    }


    public function insert($model)
    {
        return parent::insert($model);
    }

    public function getDoctorList()
    {
        return User::where([['role_id',2],['deleted_at',null]])->get();
    }

}

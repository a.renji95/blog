<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/9/2018
 * Time: 10:29 AM
 */

namespace App\Repositories\Appointment;

use App\Repositories\EloquentRepository;
use App\Models\Appointment;


class AppointmentEloquentRepository extends EloquentRepository implements AppointmentRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Appointment::class;
    }

    public function getAll()
    {
        return parent::getAll();
    }

    public function create(array $attributes)
    {
        return parent::create($attributes);
    }

    public function insert($model)
    {
        return parent::insert($model);
    }

    public function numberRecordOfAppointmentByDoctorID($doctor_id)
    {
        return Appointment::where('doctor_id',$doctor_id)->count();
    }

}

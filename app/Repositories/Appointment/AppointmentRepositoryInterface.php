<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/9/2018
 * Time: 10:27 AM
 */

namespace App\Repositories\Appointment;


interface AppointmentRepositoryInterface
{
    public function numberRecordOfAppointmentByDoctorID($doctor_id);
}

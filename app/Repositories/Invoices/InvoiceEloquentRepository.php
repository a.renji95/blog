<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/9/2018
 * Time: 10:29 AM
 */

namespace App\Repositories\Invoices;

use App\Repositories\EloquentRepository;
use App\Models\Invoice;


class InvoiceEloquentRepository extends EloquentRepository implements InvoiceEloquentInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Invoice::class;
    }

    public function getAll()
    {
        return parent::getAll();
    }



    public function create(array $attributes)
    {
        return parent::create($attributes);
    }


    public function insert($model)
    {
        return parent::insert($model);
    }

}

<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\PatientRepositoryInterface;

class PatientRepository extends BaseRepository implements PatientRepositoryInterface
{
    public function model()
    {
        return 'App\Models\User';
    }
}

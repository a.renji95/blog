<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\DoctorRepositoryInterface;

class DoctorRepository extends BaseRepository implements DoctorRepositoryInterface
{
    public function model()
    {
        return 'App\Models\User';
    }

    public function getListDoctorsAvailable($availableDoctorsID)
    {
        return $this->model::where('role_id', $this->model::ROLE_DOCTOR)->whereIn('id', $availableDoctorsID)->get();
    }

    public function getDoctor($id)
    {
        return $this->model::find($id);
    }

    public function getListDoctorsID()
    {
        return $this->model::where('role_id', $this->model::ROLE_DOCTOR)->pluck('id');
    }
}

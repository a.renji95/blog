<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\AvailableAppointmentRepositoryInterface;
use App\Models\Status;
use App\Models\AvailableAppointment;
use Carbon\Carbon;

class AvailableAppointmentRepository extends BaseRepository implements AvailableAppointmentRepositoryInterface
{
    public function model()
    {
        return AvailableAppointment::class;
    }

    public function getAppointmentByDoctorID($data)
    {
        return $this->model::where('doctor_id', $data['doctorID'])->get();
    }

    public function getAppointmentByDate($data)
    {
        return $this->model::where('date', $data['date'])->get();
    }

    public function getAppointmentByShift($data)
    {
        return $this->model::where('shift_id', $data['shift'])->get();
    }

    public function getListAvailableDoctors()
    {
        return $this->model::where('status_id', Status::STATUS_AVAILABLE)->distinct('doctor_id')->pluck('doctor_id');
    }

    public function getListAvailableDates()
    {
        return $this->model::where('status_id', Status::STATUS_AVAILABLE)->distinct('date')->pluck('date');
    }

    public function getListAvailableShifts()
    {
        return $this->model::where('status_id', Status::STATUS_AVAILABLE)->distinct('shift_id')->pluck('shift_id');
    }

    public function makeUnavailableAppointment($data)
    {
        $appointmentID = $this->model::where('doctor_id', $data['doctor'])->where('shift_id', $data['shift'])->where('date', $data['date'])->first()->id;
        $appointment = $this->model->find($appointmentID);
        $appointment->status_id = Status::STATUS_UNAVAILABLE;
        return $appointment->save();
    }

    public function updateAvailableAppointment($allShiftsID, $allDoctorsID, $dateRange)
    {
        $now = Carbon::now();
        $maxDateAvailableAppointment = Carbon::parse($this->model::max('date'));
        if ($maxDateAvailableAppointment->lt($now)) {
            for ($i = 0; $i < count($allDoctorsID); $i ++) {
                for ($j = 0; $j < count($allShiftsID); $j ++) {
                    for ($d = 0; $d < $dateRange; $d ++) {
                        $this->model::create([
                           'doctor_id' => $allDoctorsID[$i],
                            'date' => $now->copy()->addDays($d)->format('Y-m-d'),
                            'shift_id' => $allShiftsID[$j],
                            'status_id' => Status::STATUS_AVAILABLE
                        ]);
                    }
                }
            }
        } else if ($maxDateAvailableAppointment->eq($now)) {
            for ($i = 0; $i < count($allDoctorsID); $i ++) {
                for ($j = 0; $j < count($allShiftsID); $j ++) {
                    for ($d = 1; $d < $dateRange; $d ++) {
                        $this->model::create([
                            'doctor_id' => $allDoctorsID[$i],
                            'date' => $now->copy()->addDays($d)->format('Y-m-d'),
                            'shift_id' => $allShiftsID[$j],
                            'status_id' => Status::STATUS_AVAILABLE
                        ]);
                    }
                }
            }
        } else {
            $dateDiff = $maxDateAvailableAppointment->diffInDays($now);
            for ($i = 0; $i < count($allDoctorsID); $i ++) {
                for ($j = 0; $j < count($allShiftsID); $j ++) {
                    for ($d = 1; $d < ($dateRange - $dateDiff - 1); $d ++) {
                        $this->model::create([
                            'doctor_id' => $allDoctorsID[$i],
                            'date' => $maxDateAvailableAppointment->copy()->addDays($d)->format('Y-m-d'),
                            'shift_id' => $allShiftsID[$j],
                            'status_id' => Status::STATUS_AVAILABLE
                        ]);
                    }
                }
            }
        }
        return true;
    }
}

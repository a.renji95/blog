<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\StatusRepositoryInterface;

class StatusRepository extends BaseRepository implements StatusRepositoryInterface
{
    public function model()
    {
        return 'App\Models\Status';
    }
}

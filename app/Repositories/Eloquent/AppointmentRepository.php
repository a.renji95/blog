<?php

namespace App\Repositories\Eloquent;

use App\Models\Status;
use App\Repositories\Contracts\AppointmentRepositoryInterface;
use Carbon\Carbon;
use App\Models\Appointment;
use Illuminate\Support\Facades\Auth;

class AppointmentRepository extends BaseRepository implements AppointmentRepositoryInterface
{
    public function model()
    {
        return Appointment::class;
    }

    public function createAppointment($data)
    {
        $appointment = new $this->model();
        $appointment->patient_id =  Auth::id();
        $appointment->doctor_id = $data['doctor'];
        $appointment->shift_id = $data['shift'];
        $appointment->date = Carbon::parse($data['date']);
        $appointment->status_id = Status::STATUS_AVAILABLE;
        return $appointment->save();
    }

    public function getAppointmentsBooked()
    {
        return $this->model->all();
    }

    public function getHistoryAppointments($patient_id)
    {
        return $this->model::where('patient_id', $patient_id)->where('status_id', Status::STATUS_DONE)->get();
    }
}

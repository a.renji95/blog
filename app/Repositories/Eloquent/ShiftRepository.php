<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ShiftRepositoryInterface;

class ShiftRepository extends BaseRepository implements ShiftRepositoryInterface
{
    public function model()
    {
        return 'App\Models\Shift';
    }

    public function getListShifts()
    {
        return $this->model::all();
    }

    public function getShift($id)
    {
        return $this->model::find($id);
    }

    public function getListShiftsID()
    {
        return $this->model::pluck('id');
    }
}

<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\InvoicesRepositoryInterface;
use App\Models\Status;

class  InvoicesRepository extends BaseRepository implements InvoicesRepositoryInterface
{
    public function model()
    {
        return 'App\Models\Invoice';
    }

    public function getInvoices($patient_id)
    {
        return $this->model::where('patient_id', $patient_id)->where('status_id', Status::STATUS_DONE)->get();
    }
}

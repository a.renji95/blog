<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/9/2018
 * Time: 10:27 AM
 */

namespace App\Repositories\Diagnostic;


interface DiagnosticEloquentInterface
{
    public function findDiagnosticByAppointmentId($appointment_id);
}

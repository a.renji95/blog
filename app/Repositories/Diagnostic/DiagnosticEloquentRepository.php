<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/9/2018
 * Time: 10:29 AM
 */

namespace App\Repositories\Diagnostic;

use App\Repositories\EloquentRepository;
use App\Models\Diagnostic;


class DiagnosticEloquentRepository extends EloquentRepository implements DiagnosticEloquentInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Diagnostic::class;
    }

    public function getAll()
    {
        return parent::getAll();
    }



    public function create(array $attributes)
    {
        return parent::create($attributes);
    }


    public function insert($model)
    {
        return parent::insert($model);
    }

    public function findDiagnosticByAppointmentId($appointment_id)
    {
        return Diagnostic::where('appointment_id',$appointment_id)->first();
    }

}

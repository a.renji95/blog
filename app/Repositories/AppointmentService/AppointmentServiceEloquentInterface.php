<?php
/**
 * Created by PhpStorm.
 * User: hiepnguyenh
 * Date: 11/15/2018
 * Time: 5:14 PM
 */

namespace App\Repositories\AppointmentService;


interface AppointmentServiceEloquentInterface
{
    public function findAppointServiceByServiceIdAndAppointmentId($appointment_id,$service_id);
    public function removeAppointmentServiceByAppointmentId($appointment_id);
}
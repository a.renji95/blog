<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/9/2018
 * Time: 10:29 AM
 */

namespace App\Repositories\AppointmentService;

use App\Repositories\EloquentRepository;
use App\Models\AppointmentService;


class AppointmentServiceEloquentRepository extends EloquentRepository implements AppointmentServiceEloquentInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return AppointmentService::class;
    }

    public function getAll()
    {
        return parent::getAll();
    }



    public function create(array $attributes)
    {
        return parent::create($attributes);
    }

    public function insert($model)
    {
        return parent::insert($model);
    }

    public function findAppointServiceByServiceIdAndAppointmentId($appointment_id,$service_id)
    {
        return AppointmentService::where('appointment_id',$appointment_id)->where('service_id',$service_id)->first();
    }

    public function removeAppointmentServiceByAppointmentId($appointment_id)
    {
        return AppointmentService::where('appointment_id',$appointment_id)->delete();
    }
}

<?php
/**
 * Created by PhpStorm.
 * Post: HiepNH
 * Date: 10/16/2018
 * Time: 1:41 PM
 */
namespace App\Repositories;

interface DatatableProcessRepositoryInterface{
    public function fillAppointmentTable($data);
}

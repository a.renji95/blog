<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Repositories\Appointment\AppointmentEloquentRepository;
use App\Repositories\Diagnostic\DiagnosticEloquentFRepository;
use App\Repositories\Diagnostic\DiagnosticEloquentRepository;
use App\Repositories\Invoices\InvoiceEloquentRepository;
use App\Repositories\Services\ServiceEloquentRepository;
use App\Repositories\User\UserEloquentRepository;
use App\Services\Doctor\AppointmentService;
use App\Services\Doctor\DatatableService;
use Illuminate\Http\Request;
use App\Services\Datatables\DatatableProcessRepository;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Mockery\Exception;

class DoctorController extends Controller
{
    protected $datatableProcessRepository;
    protected $appointmentEloquentRepository;
    protected $serviceEloquentRepository;
    protected $diagnosticEloquentRepository;
    protected $appointmentService;
    protected $invoiceEloquentRepository;
    protected $userEloquentRepository;

    public function __construct(
        DatatableProcessRepository $datatableProcessRepository,
        AppointmentEloquentRepository $appointmentEloquentRepository,
        ServiceEloquentRepository $serviceEloquentRepository,
        DiagnosticEloquentRepository $diagnosticEloquentRepository,
        AppointmentService $appointmentService,
        InvoiceEloquentRepository $invoiceEloquentRepository,
        UserEloquentRepository $userEloquentRepository
    )
    {
        $this->appointmentEloquentRepository = $appointmentEloquentRepository;
        $this->datatableProcessRepository = $datatableProcessRepository;
        $this->serviceEloquentRepository = $serviceEloquentRepository;
        $this->diagnosticEloquentRepository = $diagnosticEloquentRepository;
        $this->appointmentService = $appointmentService;
        $this->invoiceEloquentRepository = $invoiceEloquentRepository;
        $this->userEloquentRepository = $userEloquentRepository;
    }

    public function index()
    {

        try {
            $start = Appointment::orderBy('date', 'asc')->first();
            if($start){
                $startDate = Carbon::parse($start->date)->format('m-d-Y');
            }else{
                $startDate = (new \DateTime('now'))->modify('+7 hours')->format('m-d-Y');
            }
            $end = Appointment::orderBy('date', 'desc')->first();
            if($end){
                $endDate = Carbon::parse($end->date)->format('m-d-Y');
            }else{
                $endDate = (new \DateTime('now'))->modify('+7 hours')->format('m-d-Y');
            }
        }catch (\Exception $e){
            $startDate = Carbon::parse(Carbon::now())->modify('+7 hours')->format('m-d-Y');
            $endDate = $startDate;
        }
        return view('doctor.index',compact('startDate','endDate'));
    }

    public function getAppointment(Request $request){
        $output = $this->datatableProcessRepository->fillAppointmentTable($request);
        echo json_encode($output);
    }

    public function appointmentDetailShow(Request $request){
        $appointment = $this->appointmentEloquentRepository->find($request->id);
        $services = $this->serviceEloquentRepository->getAll();
        $usedServices = array();
        $amount = 0;
        if($appointment->services){
            foreach ($appointment->services as $service){
                array_push($usedServices,$service->id);
                $amount += $service->price;
            }
        }
        $returnHTML = view('doctor.modal.AppointmentViewModal',compact('appointment','services','usedServices','amount'))->render();
        return response()->json($returnHTML);
    }

    public function appointmentSubmit(Request $request){
        $data = array();
        $data['appointment_id'] = $request->appointment_id;
        $data['diagnostic'] = $request->diagnostic;
        $data['services'] = $request->services;
        Log::info($data['services']);
        $output = $this->appointmentService->addDiagnosticService($data);
        echo json_encode($output);
    }

    public function getServicesAmount(Request $request){
        $services = $request->services;
        $amount = 0;
        if($services){
            foreach ($services as $service_id){
                $service = $this->serviceEloquentRepository->find($service_id);
                if($service){
                    $amount += $service->price;
                }
            }
        }
        $output = '<label class="text-success">Amount: '.number_format($amount).'đ</label>';
        echo json_encode($output);
    }

    public function paymentConfirm(Request $request){
        $data = array(
            'appointment_id' => $request->appointment_id
        );
        $output = $this->appointmentService->paymentConfirm($data);
        echo json_encode($output);
    }

    public function printInvoice(Request $request){
        try{
            $appointment_id = $request->appointment_id;
            $appointment = $this->appointmentEloquentRepository->find($appointment_id);
        }catch (Exception $e){
            return redirect()->back();
        }
        return view('doctor.invoice_print',compact('appointment'));
    }

    public function chatRoomView(){
        $doctors = $this->userEloquentRepository->getDoctorList();
        return view('doctor.chat_room',compact('doctors'));
    }
}

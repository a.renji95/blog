<?php

namespace App\Http\Controllers;

use App\Services\AppointmentService;
use Illuminate\Http\Request;
use App\Services\PatientService;
use Illuminate\Support\Facades\Log;

class PatientController extends Controller
{
    protected $patientService;
    protected $appointmentService;

    public function __construct(
        PatientService $patientService,
        AppointmentService $appointmentService
    )
    {
        $this->patientService = $patientService;
        $this->appointmentService = $appointmentService;
    }

    public function index()
    {
        $appointmentsBooked = $this->appointmentService->getAppointmentsBooked();
        return view('patient.index', compact('appointmentsBooked'));
    }

    public function getAppointments ()
    {
        $appointments = $this->patientService->getAppointments();
        return view('patient.reserve', compact('appointments'));
    }

    public function makeAppointment(Request $request)
    {
        if ($request->elementChange == 'doctorID') {
            $appointments= [];
            $result = $this->appointmentService->getAvailableAppointmentByDoctorID($request->all());
            $appointments['shifts'] = $result['availableShifts'];
            $appointments['dates'] = $result['availableDates'];
            $appointments['doctors'] = $result['availableDoctors'];
            $shiftBooked = $result['shiftBooked'];
            $dateBooked = $result['dateBooked'];
            $doctorBooked = $request->doctorID;
            $returnHTML = view('elements.makeAppointment', compact('shiftBooked', 'dateBooked', 'doctorBooked', 'appointments'))->render();
        } else if($request->elementChange == 'date') {
            $appointments= [];
            $result = $this->appointmentService->getAvailableAppointmentByDate($request->all());
            $appointments['shifts'] = $result['availableShifts'];
            $appointments['dates'] = $result['availableDates'];
            $appointments['doctors'] = $result['availableDoctors']; ;
            $shiftBooked = $result['shiftBooked'];
            $dateBooked = $request->date;
            $doctorBooked = $result['doctorBooked'];
            $returnHTML = view('elements.makeAppointment', compact('shiftBooked', 'dateBooked', 'doctorBooked', 'appointments'))->render();
        } else {
            $appointments= [];
            $result = $this->appointmentService->getAvailableAppointmentByShift($request->all());
            $appointments['shifts'] = $result['availableShifts'];
            $appointments['dates'] = $result['availableDates'];
            $appointments['doctors'] = $result['availableDoctors'];
            $shiftBooked = $request->shift;
            $dateBooked = $result['dateBooked'];
            $doctorBooked = $result['doctorBooked'];
            $returnHTML = view('elements.makeAppointment', compact('shiftBooked', 'dateBooked', 'doctorBooked', 'appointments'))->render();
        }
        return response()->json($returnHTML);
    }

    public function createAppointment(Request $request){
        $this->appointmentService->createAppointment($request->all());
        return redirect()->route('patient.index');
    }

    public function history()
    {
        $histories = $this->appointmentService->getHistoryAppointments();
        return view('patient.history', compact('histories'));
    }

    public function invoices()
    {
        $invoices = $this->appointmentService->getInvoices();
        return view('patient.invoice', compact('invoices'));
    }
}

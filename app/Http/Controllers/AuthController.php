<?php

namespace App\Http\Controllers;

use App\Services\User\UserRegisterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\Validators;
use Illuminate\Support\Facades\Log;
use App\Providers\SweetAlertServiceProvider;
use SebastianBergmann\Environment\Console;

class AuthController extends Controller
{
    protected $userRegisterService;
    protected $validatorService;

    public function __construct(
        UserRegisterService $userRegisterService,
        Validators\ValidatorService $validatorService
    ){
        $this->userRegisterService = $userRegisterService;
        $this->validatorService = $validatorService;
    }

    public function registerProcess(Request $request)
    {
        $this->validatorService->registerValidate($this,$request);
        $requestParams = array();
        $requestParams['name'] = trim($request->name);
        $requestParams['email'] = trim($request->email);
        $requestParams['password'] = bcrypt($request->password);
        if ($this->userRegisterService->register($requestParams)) {
            Auth::attempt(['email'=>trim($request->email), 'password'=>trim($request->password)]);
            return back()->with('success', 'Your new account have been created!!');
        }
        return back()->withErrors('Something is wrong!!!');
    }

    public function loginProcess(Request $request){
        Log::info('login process');
        if(Auth::attempt(['email'=>trim($request->email), 'password'=>trim($request->password)])){
            if(!is_null(Auth::user()->deleted_at)){
                Auth::logout();
                return back()->withErrors('Your account is banded!!!');
            }
            switch (Auth::user()->role->role){
                case 'patient' :
                    return redirect()->route('patient.index');
                    break;
                case 'doctor' :
                    return redirect()->route('doctor.index');
                    break;
                default:
                    return redirect()->route('patient.index');
                    break;
            }
        }
        return back()->withErrors('Your account not correct!!!');
    }

    public function logout(){
        Auth::logout();
        return redirect(route('login'));
    }
}

<?php

namespace App\Models;

class User extends BaseModel
{
    const ROLE_OWNER = 1;
    const ROLE_DOCTOR = 2;
    const ROLE_PATIENT = 3;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'address', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->hasOne(UserRole::class,'id','role_id');
    }

    public function appointmentsOfPatient(){
        return $this->hasMany(Appointment::class,'patient_id','id');
    }

    public function appointmentsOfDoctor(){
        return $this->hasMany(Appointment::class,'doctor_id','id');
    }
}

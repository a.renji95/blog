<?php

namespace App\Models;

class Appointment extends BaseModel
{
    protected $table = 'appointments';

    protected $fillable = [
        'patient_id', 'doctor_id', 'shift_id', 'date', 'status_id', 'description'
    ];

    public function patient(){
        return $this->belongsTo(User::class,'patient_id','id');
    }

    public function doctor(){
        return $this->belongsTo(User::class,'doctor_id','id');
    }

    public function status(){
        return $this->belongsTo(Status::class,'status_id','id');
    }

    public function shift(){
        return $this->belongsTo(Shift::class,'shift_id','id');
    }

    public function diagnostic(){
        return $this->belongsTo(Diagnostic::class,'id','appointment_id');
    }

    public function appointment_services(){
        return $this->hasMany(AppointmentService::class,'appointment_id','id');
    }

    public function services(){
        return $this->hasManyThrough(Service::class,AppointmentService::class,'appointment_id','id','id','service_id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'id', 'appointment_id');
    }

}

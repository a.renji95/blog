<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvailableAppointment extends Model
{
    protected $table = 'available_appointments';

    protected $fillable = [
        'doctor_id', 'date', 'status_id', 'shift_id'
    ];
}

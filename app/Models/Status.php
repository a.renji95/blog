<?php

namespace App\Models;

class Status extends BaseModel
{
    const STATUS_AVAILABLE = 1;
    const STATUS_WAITING = 2;
    const STATUS_CANCEL = 3;
    const STATUS_DONE = 4;
    const STATUS_UNAVAILABLE = 5;

    protected $table = 'status';

    protected $fillable = [
        'name'
    ];

    public function appointments(){
        return $this->hasMany(Appointment::class,'status_id','id');
    }
}

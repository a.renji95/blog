<?php

namespace App\Models;

class Invoice extends BaseModel
{
    protected $table = 'invoices';

    protected $fillable = [
        'status_id', 'patient_id', 'appointment_id', 'amount', 'payment_date',
    ];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class, 'appointment_id', 'id');
    }
}

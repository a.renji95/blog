<?php

namespace App\Models;


class AppointmentService extends BaseModel
{
    protected $table = 'appointment_services';

    protected $fillable = [
        'appointment_id', 'service_id'
    ];

    public $timestamps = false;

    public function appointment(){
        return $this->belongsTo(Appointment::class,'appointment_id','id');
    }


}

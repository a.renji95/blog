<?php

namespace App\Models;

class Shift extends BaseModel
{
    protected $table = 'shifts';

    protected $fillable = [
        'startTime', 'endTime'
    ];

    public function appointments(){
        return $this->hasMany(Appointment::class,'shift_id','id');
    }
}

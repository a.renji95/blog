<?php

namespace App\Models;

class Diagnostic extends BaseModel
{
    protected $table = 'diagnostics';

    protected $fillable = [
        'description', 'appointment_id'
    ];

    public function appointment(){
        return $this->hasOne(Appointment::class,'id','appointment_id');
    }
}

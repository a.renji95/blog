<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $repositories = [
            'Patient',
            'Doctor',
            'Shift',
            'Appointment',
            'Status',
            'AvailableAppointment',
            'Invoices'
        ];

        foreach ($repositories as $repository) {
            $this->app->bind("App\\Repositories\\Contracts\\{$repository}RepositoryInterface", "App\\Repositories\\Eloquent\\{$repository}Repository");
        }

        $this->app->singleton(
            \App\Repositories\User\UserRepositoryInterface::class,
            \App\Repositories\User\UserEloquentRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Appointment\AppointmentRepositoryInterface::class,
            \App\Repositories\Appointment\AppointmentEloquentRepository::class
        );

        $this->app->singleton(
            \App\Repositories\DatatableRequestRepositoryInterface::class,
            \App\Services\Datatables\DatatableRequestRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Services\ServiceRepositoryInterface::class,
            \App\Repositories\Services\ServiceEloquentRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Diagnostic\DiagnosticEloquentInterface::class,
            \App\Repositories\Diagnostic\DiagnosticEloquentRepository::class
        );
        $this->app->singleton(
            \App\Repositories\AppointmentService\AppointmentServiceEloquentInterface::class,
            \App\Repositories\AppointmentService\AppointmentServiceEloquentRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Invoices\InvoiceEloquentInterface::class,
            \App\Repositories\Invoices\InvoiceEloquentRepository::class
        );
    }
}

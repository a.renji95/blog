<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Login.login');
})->name('login');


Route::post('/patient-login','AuthController@loginProcess')->name('patient.login.submit');

Route::middleware(['patient'])->group(function () {
    Route::get('patient', 'PatientController@index')->name('patient.index');
    Route::get('patient/get-appointments', 'PatientController@getAppointments')->name('patient.get.appointments');
    Route::post('patient/make-appointments', 'PatientController@makeAppointment')->name('patient.make.appointment');
    Route::post('patient/create-appointment', 'PatientController@createAppointment')->name('patient.create.appointment');
    Route::get('patient/history', 'PatientController@history')->name('patient.history');
    Route::get('patient/invoices', 'PatientController@invoices')->name('patient.invoices');
});

Route::get('/patient-register', function () {
    return view('Register.register');
})->name('patient.register');

Route::post('/patient-register','AuthController@registerProcess')->name('patient.register.submit');

//Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/doctor-home', 'DoctorController@index')->name('doctor.index')->middleware('doctor');
Route::get('/patient-logout','AuthController@logout')->name('patient.logout')->middleware('patient');
Route::get('/doctor-logout','AuthController@logout')->name('doctor.logout')->middleware('doctor');
Route::post('/doctor-appointment-load','DoctorController@getAppointment')->name('doctor.getListAppointment')->middleware('doctor');
Route::post('/doctor-appointment-modal','DoctorController@appointmentDetailShow')->name('doctor.AppointmentViewModalShow')->middleware('doctor');
Route::post('/doctor-appointment-submit','DoctorController@appointmentSubmit')->name('doctor.appointment.submit')->middleware('doctor');
Route::post('/doctor-services-amount','DoctorController@getServicesAmount')->name('doctor.service.amount')->middleware('doctor');
Route::post('/doctor-appointment-payment-confirm','DoctorController@paymentConfirm')->name('doctor.payment.submit')->middleware('doctor');
Route::get('/doctor-invoice-print/{appointment_id}','DoctorController@printInvoice')->name('printInvoice')->middleware('doctor');
Route::get('/doctor-chat-room-view','DoctorController@chatRoomView')->name('doctor.chat_room.view')->middleware('doctor');




